#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass docbook
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 0
\use_package amssymb 0
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Libpwmd Tutorial
\end_layout

\begin_layout Date
Sep-21-2016
\end_layout

\begin_layout Author
Ben Kibbey
\begin_inset Foot
status open

\begin_layout Plain Layout
bjk@luxsci.net
\end_layout

\end_inset


\end_layout

\begin_layout Author
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
This is a tutorial showing the basic usage of libpwmd
\begin_inset Foot
status open

\begin_layout Plain Layout
https://gitlab.com/bjk/libpwmd/wikis
\end_layout

\end_inset

.
 Libpwmd
\emph on
 
\emph default
is a library making it easy for applications to use pwmd
\begin_inset Foot
status open

\begin_layout Plain Layout
https://gitlab.com/bjk/pwmd/wikis
\end_layout

\end_inset

 (
\emph on
Password Manager Daemon).
 Password Manager Daemon
\emph default
 is a universal data server.
 It began as a way to keep track of passwords for accounts like email and
 websites but has evolved to store anything you want.
 There are other password management tools but pwmd has a couple of distinguishi
ng features:
\end_layout

\begin_layout Itemize
It does not depend on a desktop environment but has the ability for applications
 to connect to it like a desktop solution provides.
\end_layout

\begin_layout Itemize
Some portion of a stored data can be shared with another portion in the
 same data file.
 This feature behaves a lot like a symbolic link on a file system, XML entities,
 or HTML targets if you're familiar with those, but implemented in a different
 way.
 This means less duplication of content.
 See 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Linking-elements"

\end_inset

 for details.
\end_layout

\begin_layout Itemize
The XML document is OpenPGP encrypted and signed using an existing or newly
 generated key pair.
 Symmetric encryption is also supported as well as smartcards.
 All crypto operations are done using GpgME which in turn uses gnupg.
\end_layout

\begin_layout Standard
Other features include:
\end_layout

\begin_layout Itemize
Multi-threaded.
 More than one client may access the same data file while optionally locking
 out other clients.
\end_layout

\begin_layout Itemize
Secure memory management.
 Pwmd will zero out memory before freeing it and the contents of the cached
 document are encrypted.
\end_layout

\begin_layout Itemize
Remote connections over TLS with client certificates used as authentication.
\end_layout

\begin_layout Itemize
Remote connections over SSH (some configuration needed).
\end_layout

\begin_layout Itemize
ACL's for each element in an element path, listening socket and filename.
\end_layout

\begin_layout Section
File Format and Element Paths
\end_layout

\begin_layout Standard
The document is in XML format and is manipulated by commands sent from a
 client.
 Commands that access previously stored data take what is called an 
\emph on
element path
\emph default
 as an argument.
 An element path is a character string containing element names representing
 branches of an element tree.
 The branches are separated with a TAB (ASCII 0x09) character.
 Why a TAB? So other characters can be used in the element name.
 If for example a '/' were used as the separator, then a URL (i.e., http://sf.net/)
 could not be used as an element name since it would be an XML syntax error.
 For the rest of this tutorial, when you see <TAB>, replace it with a real
 TAB character.
\end_layout

\begin_layout Standard
For example, the element path "root<TAB>child<TAB>last" has the following
 element tree structure:
\end_layout

\begin_layout Code
<root>
\end_layout

\begin_layout Code
    <child>
\end_layout

\begin_layout Code
        <last>
\end_layout

\begin_layout Code
Content or XML CDATA of the "last" element.
\end_layout

\begin_layout Code
        </last>
\end_layout

\begin_layout Code
    </child>
\end_layout

\begin_layout Code
</root>
\end_layout

\begin_layout Standard
I should say that the XML structure that pwmd uses is a little more complicated.
 It really looks like the following internally, but we will use the above
 format in this tutorial for simplicity:
\end_layout

\begin_layout Code
<element _name="root">
\end_layout

\begin_layout Code
    <element _name="child">
\end_layout

\begin_layout Code
        <element _name="last">
\end_layout

\begin_layout Code
Content or CDATA of the "last" element.
\end_layout

\begin_layout Code
        </element>
\end_layout

\begin_layout Code
    </element>
\end_layout

\begin_layout Code
</element>
\end_layout

\begin_layout Standard
Every element created has an element named 
\emph on
element
\emph default
 with an attribute 
\emph on
_name
\emph default
 associated with it.
 The value of the 
\emph on
_name
\emph default
 attribute is what an element in an element path refers to.
 It is done this way so that a wider range of characters can be used in
 an element name while maintaining valid XML.
 In fact the only restriction of an element name is that it not contain
 whitespace characters.
\end_layout

\begin_layout Standard
There is one other difference from your normal XML document in that only
 the first match of an element is considered for the current element tree
 depth.
 When an element of the current depth of an element tree is found, the next
 element in the element path is searched for beginning at the child node
 of the found element.
 From the example above:
\end_layout

\begin_layout Code
<root>
\end_layout

\begin_layout Code
    <child>
\end_layout

\begin_layout Code
        <last>
\end_layout

\begin_layout Code
Content or XML CDATA of the "last" element.
\end_layout

\begin_layout Code
        </last>
\end_layout

\begin_layout Code
    </child>
\end_layout

\begin_layout Code
    <child>
\end_layout

\begin_layout Code
This element will never be reached.
\end_layout

\begin_layout Code
    </child>
\end_layout

\begin_layout Code
</root>
\end_layout

\begin_layout Standard
The second "<child>" element is never reached because it has the same name
 as its sibling element 
\begin_inset Quotes eld
\end_inset

above
\begin_inset Quotes erd
\end_inset

 it.
\end_layout

\begin_layout Section
Connecting to PWMD
\end_layout

\begin_layout Standard
You will need a 
\emph on
pwmd
\emph default
 client to send commands to the pwmd server.
 
\emph on
libpwmd
\emph default
 includes a client named 
\emph on
pwmc
\emph default
.
 It is command-line based and there are not any fancy graphics, but it is
 good for understanding how 
\emph on
pwmd
\emph default
 commands are processed.
 If you want a more user friendly client that resembles a file manager and
 has a point and click interface, try QPwmc 
\begin_inset Foot
status open

\begin_layout Plain Layout
https://gitlab.com/bjk/qpwmc/wikis
\end_layout

\end_inset

 which uses the Qt4 or Qt5 toolkit and is also a full featured client.
\end_layout

\begin_layout Standard
In this tutorial we will use the 
\emph on
pwmc
\emph default
 client included with libpwmd.
 
\emph on
pwmc
\emph default
 has two modes that commands are read from: interactive and stdin.
 The interactive mode uses the readline library and has command history,
 while reading from standard input (stdin) makes shell scripting and automation
 easy.
 For the examples, we will be connecting to the default local unix domain
 socket (UDS) that pwmd waits for connections on and use interactive mode.
 Remote connections are also possible over TLS or an SSH channel but those
 are not covered here.
 Read the pwmc(1) manual page for details about how to do that.
\end_layout

\begin_layout Standard
The example filename we will use is "datafile".
 It is initially a non-existent file but it will be created once we have
 saved to it.
 Now let us connect to the server in interactive mode and open the data
 file:
\end_layout

\begin_layout Code
$ pwmc datafile
\end_layout

\begin_layout Code
Connected.
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\begin_layout Standard
The "pwmc>" prompt is a readline prompt that has command history and can
 also do filename completion when doing a dot command that requires a filename
 on the local filesystem.
\end_layout

\begin_layout Section
Commands
\end_layout

\begin_layout Standard
There are two different types of commands: client commands and protocol
 commands.
 The client commands are 
\emph on
pwmc
\emph default
 specific and only available to the 
\emph on
pwmc
\emph default
 client.
 Protocol commands are commands sent from any client and to the 
\emph on
pwmd
\emph default
 server.
 All pwmc client commands are prefixed with a dot '.' followed by the command
 name.
 Protocol commands are sent without any dot prefix.
 To see the available pwmc and protocol commands, send the .help or HELP
 commands:
\end_layout

\begin_layout Code
pwmc:datafile> .help
\end_layout

\begin_layout Standard
or
\end_layout

\begin_layout Code
pwmc:datafile> help
\end_layout

\begin_layout Standard
To store some data in an element path or to create an element path, you
 will need to know what element path to create.
 It is up to you how you want your data organized.
 If for example you will be storing account information it may be good to
 categorize what the account is for: email, instant messaging, blogging,
 etc.
 An application that uses libpwmd may require that a certain element path
 exists.
 Refer to that applications documentation to determine what element paths
 need to be created.
\end_layout

\begin_layout Standard
In the following example we will setup mail server element paths which can
 be used for other applications requiring a mail server configuration.
 First, lets create the hostname element path:
\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
Press CTRL-D to send the current line.
 Press twice to end.
 DATA:
\end_layout

\begin_layout Standard
The STORE command is a 
\emph on
pwmd
\emph default
 protocol command.
 This, and a few other commands, use what is called a 
\emph on
server inquire
\emph default
 to retrieve additional command parameters from the client while other commands
 require only the command itself with its command arguments.
 The server inquire will wait for the client to finish sending its' data
 before completing the command.
 When responding to a server inquire in pwmc, pwmc will show a message informing
 you that it is waiting for data to be sent.
 Enter the element path and its content, then press <CTRL-D> twice to finish
 sending the data and to let 
\emph on
pwmd
\emph default
 complete the command:
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>hostname<TAB>imap.server.com<CTRL-D><CTRL-D>
\end_layout

\begin_layout Standard
Remember that you should replace <TAB> in the examples with a real TAB character.
 After pressing the first <CTRL-D>, the characters that were entered are
 sent to 
\emph on
pwmd
\emph default
 and the inquire continues.
 To terminate the inquire and finish sending data for the command press
 <CTRL-D> a second time.
\end_layout

\begin_layout Standard
If you were to have pressed <RETURN> before any <CTRL-D> then the inquired
 line would have been sent in full including a newline character.
 Since in this example a newline character in a hostname is not a valid
 hostname, this is not what we want.
\end_layout

\begin_layout Standard
We have just created an element path whose XML structure looks like the
 following:
\end_layout

\begin_layout Code
<email>
\end_layout

\begin_layout Code
    <isp>
\end_layout

\begin_layout Code
        <IMAP>
\end_layout

\begin_layout Code
            <hostname>imap.server.com</hostname>
\end_layout

\begin_layout Code
        </IMAP>
\end_layout

\begin_layout Code
    </isp>
\end_layout

\begin_layout Code
</email>
\end_layout

\begin_layout Standard
The GET protocol command returns the value, or content, of the last element
 of an element path.
 To retrieve the hostname of the element path we just created, do:
\end_layout

\begin_layout Code
pwmc:datafile> GET email<TAB>isp<TAB>IMAP<TAB>hostname
\end_layout

\begin_layout Code
imap.server.com
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\begin_layout Standard
Let us create the rest of the needed elements:
\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>port<TAB>993<CTRL-D><CTRL-D>
\end_layout

\begin_layout Code

\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>ssl<TAB>1<CTRL-D><CTRL-D>
\end_layout

\begin_layout Code

\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
email<TAB>isp<TAB>username<TAB>myusername<CTRL-D><CTRL-D>
\end_layout

\begin_layout Code

\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
email<TAB>isp<TAB>password<TAB>mypassword<CTRL-D><CTRL-D>
\end_layout

\begin_layout Standard
Now the element structure for the "email" element looks like this:
\end_layout

\begin_layout Code
<email>
\end_layout

\begin_layout Code
    <isp>
\end_layout

\begin_layout Code
        <IMAP>
\end_layout

\begin_layout Code
              <hostname>imap.server.com</hostname>
\end_layout

\begin_layout Code
              <port>993</port>
\end_layout

\begin_layout Code
              <ssl>1</ssl>
\end_layout

\begin_layout Code
        </IMAP>
\end_layout

\begin_layout Code
        <username>myusername</username>
\end_layout

\begin_layout Code
        <password>mypassword</password>
\end_layout

\begin_layout Code
    </isp>
\end_layout

\begin_layout Code
</email>
\end_layout

\begin_layout Standard
If you wanted to change your password (after changing it on the mail server,
 of course) just do as you did when initially creating the password element
 path.
 The new content will overwrite the existing content:
\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
email<TAB>isp<TAB>password<TAB>newpassword<CTRL-D><CTRL-D>
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\begin_layout Standard
An application using libpwmd that requires mail server information now has
 the basic information it needs.
 It may require more elements and they can be created just as these elements
 have been.
 The only thing left to do now is to save the changes:
\end_layout

\begin_layout Code
pwmc:datafile> <CTRL-D>
\end_layout

\begin_layout Standard
After pressing <CTRL-D> a prompt will be shown asking what to do next.
 Press 
\begin_inset Quotes eld
\end_inset

s
\begin_inset Quotes erd
\end_inset

 to save what we have created.
 This will generate a new encryption and signing key pair by default.
 If you would rather use an existing encryption and signing key or use symmetric
 encryption, you will need to use the .save pwmc command along with the required
 SAVE protocol command options.
 The .save command lets libpwmd set some things that may be needed for the
 current connection such as pinentry settings.
 Type:
\end_layout

\begin_layout Code
pwmc:datafile> help save
\end_layout

\begin_layout Standard
to see SAVE syntax and options.
\end_layout

\begin_layout Standard
When the save has completed the encrypted data file has been written to
 disk and is also stored in pwmd's file cache.
 A cached document is the same document on disc that is stored in memory.
 This means that the next time the document is opened a passphrase won't
 be required until pwmd's cache timer removes it from the cache.
 By default, the cache timeout for each data file is 600 seconds, or 10
 minutes.
 This setting can be changed in pwmd's configuration and set independently
 for each file and can also be set to not cache the file at all or to cache
 it indefinately.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:Linking-elements"

\end_inset

Linking elements
\end_layout

\begin_layout Standard
One distinguishing feature of pwmd is the ability to share data of one element
 path with another.
 If for example your ISP lets you host a blog on their server and you use
 a blogging client that can use libpwmd for authentication details, you
 can share authentication information with the email example above when
 the account details are the same.
 When element linking is used, this avoids the need to change the content
 for both the blogging and email password elements.
\end_layout

\begin_layout Standard
This is done by setting a special 
\begin_inset Quotes eld
\end_inset

_target
\begin_inset Quotes erd
\end_inset

 attribute for an element.
 It behaves similarly to the HTML 
\begin_inset Quotes eld
\end_inset

target
\begin_inset Quotes erd
\end_inset

 attribute or XML entities or a symbolic link on a filesystem.
 
\end_layout

\begin_layout Standard
Let's create an example blogging element path:
\end_layout

\begin_layout Code
pwmc:datafile> STORE
\end_layout

\begin_layout Code
blog<TAB>isp<TAB>hostname<TAB>blog.myisp.com<CTRL-D><CTRL-D>
\end_layout

\begin_layout Code

\end_layout

\begin_layout Code
pwmc:datafile> ATTR SET _target blog<TAB>isp<TAB>username email<TAB>isp<TAB>user
name
\end_layout

\begin_layout Code
pwmc:datafile> ATTR SET _target blog<TAB>isp<TAB>password email<TAB>isp<TAB>pass
word
\end_layout

\begin_layout Standard
Now each access of the "blog/isp/username" and "blog/isp/password" element
 paths, which were created if they did not already exist, will point to
 "email/isp/username" and "email/isp/password", respectively.
 To retrieve the value or content of an element that contains a "_target"
 attribute, just use the GET command as you would for any other element:
\end_layout

\begin_layout Code
pwmc:datafile> GET blog<TAB>isp<TAB>password
\end_layout

\begin_layout Code
mypassword
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\begin_layout Standard
A "_target" attribute may also refer to another element with a "_target"
 attribute.
 Every "_target" attribute will be followed until there are no others to
 resolve.
 To get the real element path and resolve all "_target" attributes, use
 the REALPATH protocol command:
\end_layout

\begin_layout Code
pwmc:datafile> REALPATH blog<TAB>isp<TAB>password
\end_layout

\begin_layout Code
email<TAB>isp<TAB>password
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\begin_layout Standard
Using the LIST command is useful to show the element structure of a document:
\end_layout

\begin_layout Code
pwmc:datafile> LIST
\end_layout

\begin_layout Code
email
\end_layout

\begin_layout Code
blog
\end_layout

\begin_layout Code
pwmc:datafile> LIST email
\end_layout

\begin_layout Code
email
\end_layout

\begin_layout Code
email<TAB>isp
\end_layout

\begin_layout Code
email<TAB>isp<TAB>username
\end_layout

\begin_layout Code
email<TAB>isp<TAB>password
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>hostname
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>port
\end_layout

\begin_layout Code
email<TAB>isp<TAB>IMAP<TAB>ssl
\end_layout

\begin_layout Code
pwmc:datafile> LIST blog
\end_layout

\begin_layout Code
blog
\end_layout

\begin_layout Code
blog<TAB>isp
\end_layout

\begin_layout Code
blog<TAB>isp<TAB>hostname
\end_layout

\begin_layout Code
blog<TAB>isp<TAB>username
\end_layout

\begin_layout Code
blog<TAB>isp<TAB>password
\end_layout

\begin_layout Code
pwmc:datafile>
\end_layout

\end_body
\end_document
