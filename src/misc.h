/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef MISC_H
#define MISC_H

#include "types.h"

#ifndef __MINGW32__
#include <pwd.h>

char *_expand_homedir (char *str, struct passwd *pw);
char *_getpwuid (struct passwd *pwd);
#endif

char *_percent_escape (const char *atext);
gpg_error_t parse_hostname_common (const char *str, char **host, int *port);
char *bin2hex (const unsigned char *data, size_t len);
gpg_error_t set_non_blocking (assuan_fd_t fd, int n);

#endif
