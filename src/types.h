/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef TYPES_H
#define TYPES_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libpwmd.h>
#include <pthread.h>
#include <assuan.h>

#ifdef WITH_SSH
#include "ssh.h"
#endif

#ifdef WITH_GNUTLS
#include "tls.h"
#endif

#ifdef ENABLE_NLS
#    ifdef HAVE_LOCALE_H
#    include <locale.h>
#    endif
#include "gettext.h"
#define N_(msgid)	dgettext("libpwmd", msgid)
#else
#define N_(msgid)	(msgid)
#endif

#define N_ARRAY(a)	(sizeof(a)/sizeof(a[0]))

typedef enum
{
  PWMD_IPV6, PWMD_IPV4, PWMD_IP_ANY
} pwmd_ip_version_t;

#if defined(WITH_SSH) || defined(WITH_GNUTLS)

#define TCP_FLAG_DNS_FINISHED 0x01

struct tcp_s
{
  char *host;
  unsigned port;
  gpg_error_t rc;
  struct addrinfo *addrs;
  struct addrinfo *addr;
  pthread_cond_t dns_cond;
  pthread_mutex_t dns_mutex;
  unsigned flags;
#ifdef WITH_SSH
  struct ssh_s *ssh;
#endif
#ifdef WITH_GNUTLS
  struct tls_s *tls;
#endif
};
#endif

/* Borrowed from g10code. */
#if __WIN64__
# define SOCKET2HANDLE(s) ((void *)(s))
# define HANDLE2SOCKET(h) ((uintptr_t)(h))
#elif __MINGW32__
# define SOCKET2HANDLE(s) ((void *)(s))
# define HANDLE2SOCKET(h) ((unsigned int)(h))
#else
# define SOCKET2HANDLE(s) (s)
# define HANDLE2SOCKET(h) (h)
#endif

#define OPT_LOCK_ON_OPEN	0x0001
#define OPT_SIGPIPE		0x0002
#define OPT_STATE_STATUS	0x0004

struct pwm_s
{
  assuan_context_t ctx;
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  struct tcp_s *tcp;
#ifdef WITH_GNUTLS
  int tls_error;
#endif
#endif
#ifdef __MINGW32__
  SOCKET fh;
#endif
  long lock_timeout;
  /* Options set with pwmd_setopt(). */
  pwmd_read_cb_t read_cb;
  void *read_cb_data;
  pwmd_write_cb_t write_cb;
  void *write_cb_data;
  int user_fd;
  pwmd_ip_version_t prot;
  pwmd_knownhost_cb_t kh_cb;
  void *kh_data;
  pwmd_passphrase_cb_t passphrase_cb;
  void *passphrase_data;
  int needs_passphrase;
  char *ssh_passphrase;
  int use_agent;
  int tls_verify;
  char *tls_priority;
  int socket_timeout;
  assuan_fd_t fd;
  int cancel;
  int connected;
#ifdef WITH_PINENTRY
  pid_t pinentry_pid;		// for local pinentry timeouts
  assuan_context_t pctx;
#endif
  char *pinentry_path;
  char *pinentry_tty;
  char *pinentry_term;
  char *pinentry_display;
  char *pinentry_lcctype;
  char *pinentry_lcmessages;
  char *pinentry_error;
  char *pinentry_prompt;
  char *pinentry_desc;
  int pinentry_tries;		// local pinentry
  int pinentry_try;		// local pinentry
  char *passphrase_hint;
  char *passphrase_info;
  char *filename;
  int pinentry_timeout;
  int current_pinentry_timeout;
  int disable_pinentry;
  int local_pinentry;
  int pinentry_disabled;        // for sending the disable command
  int pin_repeated;
  pwmd_status_cb_t status_func;
  void *status_data;
  pwmd_inquire_cb_t inquire_func;
  void *inquire_data;
  size_t inquire_total;
  size_t inquire_sent;
  size_t inquire_maxlen;
  char *bulk_id;
  size_t data_total;		// Used by the XFER status message
#ifdef WITH_QUALITY
  gpg_error_t (*_inquire_func) (void *, const char *);
  void *_inquire_data;
#endif
  char *name;
  unsigned opts;
  int override_inquire;
  void *user_data;
  unsigned version; // of pwmd
};

gpg_error_t _assuan_command (pwm_t * pwm, assuan_context_t ctx,
			     char **result, size_t * len, const char *cmd);
gpg_error_t _connect_finalize (pwm_t * pwm);
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
void free_tcp (pwm_t *);
gpg_error_t tcp_connect_common (pwm_t * pwm);
#endif

#endif
