/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef MEM_H
#define MEM_H
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef MEM_DEBUG
#define _xfree free
#define _xmalloc malloc
#define _xrealloc realloc
#define _xcalloc calloc
#else
  void _xfree (void *ptr)
     __attribute__ ((visibility ("hidden")));
  void *_xmalloc (size_t size)
     __attribute__ ((visibility ("hidden")));
  void *_xrealloc (void *ptr, size_t size)
     __attribute__ ((visibility ("hidden")));
  void *_xcalloc (size_t nmemb, size_t size)
     __attribute__ ((visibility ("hidden")));
  void *_xrealloc_gpgrt (void *, size_t)
     __attribute__ ((visibility ("hidden")));
#endif
void wipememory (void *ptr, int c, size_t len);

#ifdef __cplusplus
}
#endif

#endif
