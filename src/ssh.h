/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef SSH_H
#define SSH_H

#include <libssh2.h>
#include <sys/types.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

struct ssh_s
{
  char *username;
  char *known_hosts;
  char *identity;
  char *identity_pub;
  LIBSSH2_SESSION *session;
  LIBSSH2_CHANNEL *channel;
  LIBSSH2_KNOWNHOSTS *kh;
  LIBSSH2_AGENT *agent;
  struct libssh2_knownhost *hostent;
  char *hostkey;
  int timeout;
  struct timeval elapsed;
};

void _free_ssh_conn (struct ssh_s *conn);
gpg_error_t _setup_ssh_session (pwm_t * pwm);
gpg_error_t _do_ssh_connect (pwm_t * pwm, const char *host, int port,
			     const char *identity, const char *user,
			     const char *known_hosts);
gpg_error_t _parse_ssh_url (const char *str, char **host, int *port,
			    char **user);
ssize_t write_hook_ssh (struct ssh_s *, assuan_fd_t fd, const void *data,
			size_t len);
ssize_t read_hook_ssh (struct ssh_s *, assuan_fd_t fd, void *data,
		       size_t len);

#endif
