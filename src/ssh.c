/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifndef __MINGW32__
#include <pwd.h>
#ifdef HAVE_ERR_H
#    include <err.h>
#endif
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif
#ifdef HAVE_NETINET_IN_H
#    include <netinet/in.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#    include <sys/socket.h>
#endif
#ifdef HAVE_ARPA_INET_H
#    include <arpa/inet.h>
#endif
#endif

#include <libpwmd.h>

#ifndef LINE_MAX
#define LINE_MAX	2048
#endif

#include "types.h"
#include "misc.h"
#include "ssh.h"

#define INTERVAL 50000
#ifdef __MINGW32__
#define TEST_TIMEOUT(pwm, ssh, n) do {	 			\
    if (pwm->cancel)						\
      n = LIBSSH2_ERROR_TIMEOUT;				\
    else if (n == LIBSSH2_ERROR_EAGAIN)				\
      {								\
        if (ssh->elapsed.tv_usec + INTERVAL >= 1000000L)	\
          {							\
            ssh->elapsed.tv_sec++;				\
            ssh->elapsed.tv_usec = 0;				\
          }							\
        if (ssh->elapsed.tv_sec >= pwm->socket_timeout)		\
          n = LIBSSH2_ERROR_TIMEOUT;				\
        else 							\
          {							\
            struct timeval tv = { 0, INTERVAL };		\
	    __assuan_usleep (pwm->ctx, INTERVAL);		\
            ssh->elapsed.tv_usec += INTERVAL;			\
          }							\
      }								\
} while (0);
#else
#define TEST_TIMEOUT(pwm, ssh, n) do {	 			\
    if (pwm->cancel)						\
      n = LIBSSH2_ERROR_TIMEOUT;				\
    else if (n == LIBSSH2_ERROR_EAGAIN)				\
      {								\
        if (ssh->elapsed.tv_usec + INTERVAL >= 1000000L)	\
          {							\
            ssh->elapsed.tv_sec++;				\
            ssh->elapsed.tv_usec = 0;				\
          }							\
        if (ssh->elapsed.tv_sec >= pwm->socket_timeout)		\
          n = LIBSSH2_ERROR_TIMEOUT;				\
        else 							\
          {							\
            struct timeval tv = { 0, INTERVAL };		\
            select (0, NULL, NULL, NULL, &tv);			\
            ssh->elapsed.tv_usec += INTERVAL;			\
          }							\
      }								\
} while (0);
#endif

static gpg_error_t ssh_connect_finalize (pwm_t * pwm);
static gpg_error_t _setup_ssh_init (pwm_t * pwm);
static gpg_error_t _setup_ssh_authlist (pwm_t * pwm);
static gpg_error_t _setup_ssh_channel (pwm_t * pwm);
static gpg_error_t _setup_ssh_shell (pwm_t * pwm);
static gpg_error_t _setup_ssh_agent (pwm_t * pwm);

static void
close_agent (struct ssh_s *ssh)
{
  if (!ssh)
    return;

  if (ssh->agent)
    {
      libssh2_agent_disconnect (ssh->agent);
      libssh2_agent_free (ssh->agent);
      ssh->agent = NULL;
    }
}

static void
ssh_deinit (struct ssh_s *ssh)
{
  if (!ssh)
    return;

  close_agent (ssh);
  /* Fixes error messages in the pwmd log. */
  libssh2_channel_wait_closed (ssh->channel);

  if (ssh->channel)
    {
      libssh2_channel_close (ssh->channel);
      libssh2_channel_free (ssh->channel);
    }

  if (ssh->kh)
    {
      libssh2_knownhost_free (ssh->kh);
      ssh->kh = NULL;
    }

  if (ssh->session)
    {
      libssh2_session_disconnect (ssh->session, N_("libpwmd saying bye!"));
      libssh2_session_free (ssh->session);
    }

  ssh->session = NULL;
  ssh->channel = NULL;
  _free_ssh_conn (ssh);
}

ssize_t
read_hook_ssh (struct ssh_s *ssh, assuan_fd_t fd, void *data, size_t len)
{

  ssize_t ret = libssh2_channel_read (ssh->channel, data, len);

  (void)fd;
  if (ret == LIBSSH2_ERROR_TIMEOUT)
    {
      errno = ETIMEDOUT;
      return -1;
    }

  return ret;
}

ssize_t
write_hook_ssh (struct ssh_s * ssh, assuan_fd_t fd, const void *data,
		size_t len)
{
  ssize_t ret;

  (void)fd;
  /* libassuan cannot handle EAGAIN when doing writes. */
  do
    {
      struct timeval tv = { 0, INTERVAL };

      ret = libssh2_channel_write (ssh->channel, data, len);
      if (ret == LIBSSH2_ERROR_EAGAIN)
        select (0, NULL, NULL, NULL, &tv);
    }
  while (ret == LIBSSH2_ERROR_EAGAIN);

  if (ret == LIBSSH2_ERROR_TIMEOUT)
    {
      errno = ETIMEDOUT;
      return -1;
    }

  return ret;
}

void
_free_ssh_conn (struct ssh_s *ssh)
{
  if (!ssh)
    return;

  pwmd_free (ssh->username);
  ssh->username = NULL;
  pwmd_free (ssh->known_hosts);
  ssh->known_hosts = NULL;
  pwmd_free (ssh->identity);
  ssh->identity = NULL;
  pwmd_free (ssh->identity_pub);
  ssh->identity_pub = NULL;
  pwmd_free (ssh->hostkey);
  ssh->hostkey = NULL;

  if (ssh->session)
    ssh_deinit (ssh);
  else
    pwmd_free (ssh);
}

static gpg_error_t
init_ssh (pwm_t *pwm, const char *host, int port, const char *identity,
          const char *user, const char *known_hosts, int use_agent)
{
  struct tcp_s *conn;
  gpg_error_t rc = 0;
#ifndef __MINGW32__
  char *pwbuf = NULL;
  struct passwd pw;
#else

  if (!user)
    return GPG_ERR_INV_ARG;
#endif

  if (!host || !*host || (!use_agent && (!identity || !*identity))
      || (known_hosts && !*known_hosts))
    return GPG_ERR_INV_ARG;

  conn = pwmd_calloc (1, sizeof (struct tcp_s));
  if (!conn)
    return gpg_error_from_errno (ENOMEM);

  pthread_cond_init (&conn->dns_cond, NULL);
  pthread_mutex_init (&conn->dns_mutex, NULL);
  conn->ssh = pwmd_calloc (1, sizeof (struct ssh_s));
  if (!conn->ssh)
    {
      rc = gpg_error_from_errno (ENOMEM);
      goto fail;
    }

#ifndef __MINGW32__
  pwbuf = _getpwuid (&pw);
  if (!pwbuf)
    {
      rc = gpg_error_from_errno (errno);
      goto fail;
    }

  pwmd_free (conn->ssh->username);
  conn->ssh->username = pwmd_strdup (user ? user : pw.pw_name);
#else
  conn->ssh->username = pwmd_strdup (user);
#endif
  if (!conn->ssh->username)
    {
      rc = gpg_error_from_errno (ENOMEM);
      goto fail;
    }

  pwmd_free (conn->ssh->identity);
  conn->ssh->identity = NULL;
  pwmd_free (conn->ssh->identity_pub);
  conn->ssh->identity_pub = NULL;
  if (identity)
    {
#ifdef __MINGW32__
      conn->ssh->identity = pwmd_strdup (identity);
#else
      conn->ssh->identity = _expand_homedir ((char *) identity, &pw);
#endif

      if (!conn->ssh->identity)
	{
	  rc = gpg_error_from_errno (ENOMEM);
	  goto fail;
	}

      conn->ssh->identity_pub = pwmd_strdup_printf ("%s.pub",
						    conn->ssh->identity);
      if (!conn->ssh->identity_pub)
	{
	  rc = gpg_error_from_errno (ENOMEM);
	  goto fail;
	}
    }

  pwmd_free (conn->ssh->known_hosts);
#ifdef __MINGW32__
  if (known_hosts)
    conn->ssh->known_hosts = pwmd_strdup (known_hosts);
  else
    conn->ssh->known_hosts = pwmd_strdup ("known_hosts");
#else
  if (!known_hosts)
    known_hosts = "~/.ssh/known_hosts";

  conn->ssh->known_hosts = _expand_homedir ((char *) known_hosts, &pw);
#endif
  if (!conn->ssh->known_hosts)
    {
      rc = gpg_error_from_errno (ENOMEM);
      goto fail;
    }

#ifndef __MINGW32__
  pwmd_free (pwbuf);
#endif
  conn->port = port;
  conn->host = pwmd_strdup (host);
  if (!conn->host)
    {
      rc = gpg_error_from_errno (ENOMEM);
      goto fail;
    }

  pwm->tcp = conn;
  return 0;

fail:
#ifndef __MINGW32__
  if (pwbuf)
    pwmd_free (pwbuf);
#endif

  pwm->tcp = conn;
  free_tcp (pwm);
  return rc;
}

static void *
ssh_malloc (size_t size, void **data)
{
  (void)data;
  return pwmd_malloc (size);
}

static void
ssh_free (void *ptr, void **data)
{
  (void)data;
  pwmd_free (ptr);
}

static void *
ssh_realloc (void *ptr, size_t size, void **data)
{
  (void)data;
  return pwmd_realloc (ptr, size);
}

static gpg_error_t
_setup_ssh_agent (pwm_t * pwm)
{
  int n;
  struct libssh2_agent_publickey *identity = NULL;
  struct libssh2_agent_publickey *identity_prev = NULL;

  n = libssh2_agent_connect (pwm->tcp->ssh->agent);
  if (n)
    return GPG_ERR_NO_AGENT;

  n = libssh2_agent_list_identities (pwm->tcp->ssh->agent);
  if (n)
    return GPG_ERR_KEYRING_OPEN;

  n = libssh2_agent_get_identity (pwm->tcp->ssh->agent, &identity,
				  identity_prev);
  if (n > 0)
    return GPG_ERR_NO_SECKEY;
  else if (n < 0)
    return GPG_ERR_AGENT;

  for (;;)
    {
      do
	{
	  n = libssh2_agent_userauth (pwm->tcp->ssh->agent,
				      pwm->tcp->ssh->username, identity);
          TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
	}
      while (n == LIBSSH2_ERROR_EAGAIN);

      if (!n)
	break;

      if (n == LIBSSH2_ERROR_TIMEOUT)
        return pwm->cancel ? GPG_ERR_CANCELED : GPG_ERR_ETIMEDOUT;
      else if (n && n != LIBSSH2_ERROR_AUTHENTICATION_FAILED)
	return GPG_ERR_ASS_SERVER_START;

      identity_prev = identity;
      n = libssh2_agent_get_identity (pwm->tcp->ssh->agent, &identity,
				      identity_prev);

      if (n > 0)
	return GPG_ERR_NO_SECKEY;
      else if (n < 0)
	return GPG_ERR_AGENT;
    }

  return _setup_ssh_channel (pwm);
}

static gpg_error_t
obtain_password (pwm_t *pwm, char **password)
{
  size_t len;
  char *buf, *p;
  gpg_error_t rc = 0;
  char *old_desc = NULL, *old_prompt = NULL, *tmp;

  if (pwm->ssh_passphrase)
    {
      *password = pwmd_strdup (pwm->ssh_passphrase);
      if (!*password)
        rc = GPG_ERR_ENOMEM;

      return rc;
    }
  else if (pwm->passphrase_cb)
    {
      return pwm->passphrase_cb (pwm->passphrase_data, pwm->tcp->host,
                                 pwm->tcp->ssh->identity, password);
    }

  pwmd_getopt (pwm, PWMD_OPTION_PINENTRY_DESC, &tmp);
  if (tmp)
    old_desc = pwmd_strdup (tmp);

  pwmd_getopt (pwm, PWMD_OPTION_PINENTRY_PROMPT, &tmp);
  if (tmp)
    old_prompt = pwmd_strdup (tmp);

  p = strrchr (pwm->tcp->ssh->identity, '/');
  buf = pwmd_strdup_printf (N_("Please enter the passphrase for the SSH identity file \"%s\"."), p ? p+1 : pwm->tcp->ssh->identity);
  pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DESC, buf);
  pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_PROMPT, N_("Passphrase:"));
  /* In case of pwmc --no-pinentry. */
  pwm->filename = p ? p+1 : pwm->tcp->ssh->identity;
  rc = pwmd_password (pwm, "PASSPHRASE", password, &len);
  pwm->filename = NULL;
  pwmd_free (buf);
  pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DESC, old_desc);
  pwmd_free (old_desc);
  pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_PROMPT, old_prompt);
  pwmd_free (old_prompt);
  return rc;
}

/* OpenSSL supports an identity file protected with a passphrase but other
 * crypto engines do not (as far as I know). */
static gpg_error_t
_setup_ssh_authlist (pwm_t * pwm)
{
  char *userauth;
  int n;
  char *password = NULL;
  int did_pubkey = 0;
  int did_password = 0;
  int did_pubkey_password = 0;

  do
    {
      userauth = libssh2_userauth_list (pwm->tcp->ssh->session,
					pwm->tcp->ssh->username,
					strlen (pwm->tcp->ssh->username));
      n = libssh2_session_last_errno (pwm->tcp->ssh->session);
      TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
    }
  while (!userauth && n == LIBSSH2_ERROR_EAGAIN);

  if (n && n != LIBSSH2_ERROR_EAGAIN)
    {
      if (pwm->cancel)
        return GPG_ERR_CANCELED;
      return n == LIBSSH2_ERROR_TIMEOUT ? GPG_ERR_ETIMEDOUT
        : GPG_ERR_ASSUAN_SERVER_FAULT;
    }

  if (!userauth && !libssh2_userauth_authenticated (pwm->tcp->ssh->session))
    return GPG_ERR_BAD_PIN_METHOD;
  else if (!userauth)
    return _setup_ssh_channel (pwm);

  if (pwm->use_agent == 1)
    return _setup_ssh_agent (pwm);

again:
  do {
      if (strstr (userauth, "publickey") && !did_pubkey)
        {
          do
            {
              n = libssh2_userauth_publickey_fromfile (pwm->tcp->ssh->session,
                                                       pwm->tcp->ssh->username,
                                                       pwm->tcp->ssh->identity_pub,
                                                       pwm->tcp->ssh->identity,
                                                       password);
              TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
            } while (n == LIBSSH2_ERROR_EAGAIN);

          if (password)
            did_pubkey_password = 1;
        }
      else if (strstr (userauth, "password") && !did_password)
        {
          if (!password)
            {
              gpg_error_t rc = obtain_password (pwm, &password);
              if (rc)
                {
                  free_tcp (pwm);
                  return rc;
                }
            }

          do
            {
              n = libssh2_userauth_password (pwm->tcp->ssh->session,
                                             pwm->tcp->ssh->username,
                                             password);
              TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
            } while (n == LIBSSH2_ERROR_EAGAIN);

          did_password = 1;
        }
      else if (!did_pubkey && !did_password)
        n = LIBSSH2_ERROR_AUTHENTICATION_FAILED;

      if (n)
        {
          if (n == LIBSSH2_ERROR_PUBLICKEY_UNVERIFIED && !password)
            {
              gpg_error_t rc = obtain_password (pwm, &password);
              if (rc)
                {
                  free_tcp (pwm);
                  return rc;
                }

              goto again;
            }

          pwmd_free (password);
          password = NULL;
          free_tcp (pwm);
          switch (n)
            {
            case LIBSSH2_ERROR_PUBLICKEY_UNVERIFIED:
              return did_pubkey_password
                ? GPG_ERR_BAD_PASSPHRASE : GPG_ERR_UNUSABLE_SECKEY;
            case LIBSSH2_ERROR_FILE:
              return GPG_ERR_UNUSABLE_SECKEY;
            case LIBSSH2_ERROR_TIMEOUT:
              return pwm->cancel ? GPG_ERR_CANCELED : GPG_ERR_ETIMEDOUT;
            case LIBSSH2_ERROR_AUTHENTICATION_FAILED:
              return GPG_ERR_WRONG_SECKEY;
            default:
              return GPG_ERR_ASSUAN_SERVER_FAULT;
            }
        }
    } while (!libssh2_userauth_authenticated (pwm->tcp->ssh->session));

  pwmd_free (password);
  return _setup_ssh_channel (pwm);
}

static void
add_knownhost (pwm_t * pwm, const char *host, const char *key,
	       size_t len, int type, struct libssh2_knownhost **dst)
{
  char *buf;

  if (pwm->tcp->port != -1 && pwm->tcp->port != 22)
    {
      buf = pwmd_malloc (256);
      snprintf (buf, 256, "[%s]:%i", host, pwm->tcp->port);
    }
  else
    buf = pwmd_strdup (host);

  char *tbuf = pwmd_strdup_printf ("libpwmd-%li", time (NULL));
  libssh2_knownhost_addc (pwm->tcp->ssh->kh, buf, NULL, key, len, tbuf,
			  strlen (tbuf), type, dst);
  pwmd_free (tbuf);
  pwmd_free (buf);
}

static gpg_error_t
knownhosts_confirm (pwm_t *pwm, const char *host)
{
  gpg_error_t rc;
  char *old_desc, *tmp;
  char *buf = pwmd_strdup_printf(N_("Password Manager Daemon: %s\n\nWhile attempting an SSH connection to %s there was a problem verifying it's hostkey against the known and trusted hosts file because it's hostkey was not found.\n\nWould you like to treat this connection as trusted for this and future connections by adding %s's hostkey to the known hosts file?"),
                                 pwm->name ? pwm->name : "",
                                 host, host);

  rc = pwmd_getopt (pwm, PWMD_OPTION_PINENTRY_DESC, &tmp);
  if (rc)
    return rc;

  old_desc = tmp ? pwmd_strdup (tmp) : NULL;
  if (tmp && !old_desc)
    return GPG_ERR_ENOMEM;

  rc = pwmd_setopt(pwm, PWMD_OPTION_PINENTRY_DESC, buf);
  pwmd_free(buf);
  if (rc)
    return rc;

  rc = pwmd_getpin(pwm, NULL, NULL, NULL, PWMD_PINENTRY_CONFIRM);
  if (!rc || rc == GPG_ERR_CANCELED)
    pwmd_getpin(pwm, NULL, NULL, NULL, PWMD_PINENTRY_CLOSE);

  (void)pwmd_setopt(pwm, PWMD_OPTION_PINENTRY_DESC, old_desc);
  return rc;
}

static gpg_error_t
check_known_hosts (pwm_t * pwm)
{
  size_t len;
  int type;
  const char *key;
  gpg_error_t rc = 0;
  int n;
  struct libssh2_knownhost *kh;

  key = libssh2_session_hostkey (pwm->tcp->ssh->session, &len, &type);

  while (!libssh2_knownhost_get (pwm->tcp->ssh->kh, &kh, NULL))
    libssh2_knownhost_del (pwm->tcp->ssh->kh, kh);

  n = libssh2_knownhost_readfile (pwm->tcp->ssh->kh,
				  pwm->tcp->ssh->known_hosts,
				  LIBSSH2_KNOWNHOST_FILE_OPENSSH);

  if (n < 0 && n != LIBSSH2_ERROR_FILE)
    return GPG_ERR_BAD_CERT;

  n = libssh2_knownhost_checkp (pwm->tcp->ssh->kh, pwm->tcp->host,
				pwm->tcp->port, (char *) key, len,
				LIBSSH2_KNOWNHOST_TYPE_PLAIN |
				LIBSSH2_KNOWNHOST_KEYENC_RAW,
				&pwm->tcp->ssh->hostent);

  switch (type)
    {
    case LIBSSH2_HOSTKEY_TYPE_RSA:
      type = LIBSSH2_KNOWNHOST_KEY_SSHRSA;
      break;
    case LIBSSH2_HOSTKEY_TYPE_DSS:
      type = LIBSSH2_KNOWNHOST_KEY_SSHDSS;
      break;
    case LIBSSH2_HOSTKEY_TYPE_ECDSA_256:
      type = LIBSSH2_KNOWNHOST_KEY_ECDSA_256;
      break;
    case LIBSSH2_HOSTKEY_TYPE_ECDSA_384:
      type = LIBSSH2_KNOWNHOST_KEY_ECDSA_384;
      break;
    case LIBSSH2_HOSTKEY_TYPE_ECDSA_521:
      type = LIBSSH2_KNOWNHOST_KEY_ECDSA_521;
      break;
    case LIBSSH2_HOSTKEY_TYPE_ED25519:
      type = LIBSSH2_KNOWNHOST_KEY_ED25519;
      break;
    default:
      type = LIBSSH2_KNOWNHOST_KEY_UNKNOWN;
      break;
    }

  switch (n)
    {
    case LIBSSH2_KNOWNHOST_CHECK_MATCH:
      break;
    case LIBSSH2_KNOWNHOST_CHECK_NOTFOUND:
      if (!pwm->kh_cb)
        rc = knownhosts_confirm (pwm, pwm->tcp->host);
      else
	rc = pwm->kh_cb (pwm->kh_data, pwm->tcp->host, key, len);
      if (rc)
	return rc;

      /* Adds both the IP and hostname. */
      char *ip = pwmd_malloc (255);

      if (ip)
	{
	  char *tmp;

	  n = getnameinfo (pwm->tcp->addr->ai_addr,
			       pwm->tcp->addr->ai_family == AF_INET ?
			       sizeof (struct sockaddr_in) : sizeof (struct
								     sockaddr_in6),
			       ip, 255, NULL, 0, NI_NUMERICHOST);
	  if (n)
	    {
	      pwmd_free (ip);
	      return 0;
	    }

	  if (strcmp (pwm->tcp->host, ip))
	    tmp = pwmd_strdup_printf ("%s,%s", pwm->tcp->host, ip);
	  else
	    tmp = pwmd_strdup (ip);

	  if (tmp)
	    add_knownhost (pwm, tmp, key, len,
			   LIBSSH2_KNOWNHOST_TYPE_PLAIN |
			   LIBSSH2_KNOWNHOST_KEYENC_RAW | type,
			   &pwm->tcp->ssh->hostent);

	  pwmd_free (ip);
	  pwmd_free (tmp);
	}

      /* It's not an error if writing the new host file fails since
       * there isn't a way to notify the user. The hostkey is still
       * valid though. */
      char *tmp = tempnam (NULL, "khost");

      if (!tmp)
	return 0;

      if (!libssh2_knownhost_writefile (pwm->tcp->ssh->kh, tmp,
					LIBSSH2_KNOWNHOST_FILE_OPENSSH))
	{
	  char *buf;
	  FILE *ifp, *ofp = NULL;

	  buf = pwmd_malloc (LINE_MAX);
	  if (!buf)
	    {
	      unlink (tmp);
	      free (tmp);
	      return 0;
	    }

	  ifp = fopen (tmp, "r");
	  if (!ifp)
	    goto done;

	  ofp = fopen (pwm->tcp->ssh->known_hosts, "w+");
	  if (!ofp)
	    goto done;

	  while ((fgets (buf, LINE_MAX, ifp)))
	    {
	      if (fprintf (ofp, "%s", buf) < 0)
		break;
	    }

	done:
	  if (ifp)
	    fclose (ifp);
	  if (ofp)
	    fclose (ofp);

	  pwmd_free (buf);
	}

      unlink (tmp);
      free (tmp);
      return 0;
    case LIBSSH2_KNOWNHOST_CHECK_MISMATCH:
    case LIBSSH2_KNOWNHOST_CHECK_FAILURE:
      return GPG_ERR_BAD_HS_CERT_VER;
    }

  return 0;
}

static gpg_error_t
verify_hostkey (pwm_t * pwm)
{
  gpg_error_t rc;
  size_t outlen;
  char *buf;

  if (!pwm->tcp->ssh->kh)
    pwm->tcp->ssh->kh = libssh2_knownhost_init (pwm->tcp->ssh->session);
  if (!pwm->tcp->ssh->kh)
    return GPG_ERR_ENOMEM;

  rc = check_known_hosts (pwm);
  if (rc)
    return rc;

  buf = pwmd_malloc (LINE_MAX);
  if (!buf)
    return gpg_error_from_errno (ENOMEM);

  if (libssh2_knownhost_writeline (pwm->tcp->ssh->kh, pwm->tcp->ssh->hostent,
				   buf, LINE_MAX, &outlen,
				   LIBSSH2_KNOWNHOST_FILE_OPENSSH))
    {
      pwmd_free (buf);
      return gpg_error_from_errno (ENOMEM);
    }

  if (pwm->tcp->ssh->hostkey)
    pwmd_free (pwm->tcp->ssh->hostkey);
  pwm->tcp->ssh->hostkey = buf;

  return _setup_ssh_authlist (pwm);
}

static gpg_error_t
_setup_ssh_channel (pwm_t * pwm)
{
  int n;
  gpg_error_t rc = 0;

  close_agent (pwm->tcp->ssh);

  do
    {
      pwm->tcp->ssh->channel =
	libssh2_channel_open_session (pwm->tcp->ssh->session);

      n = libssh2_session_last_errno (pwm->tcp->ssh->session);
      TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
    }
  while (!pwm->tcp->ssh->channel && n == LIBSSH2_ERROR_EAGAIN);

  if (!pwm->tcp->ssh->channel || (n && n != LIBSSH2_ERROR_EAGAIN))
    {
      rc = GPG_ERR_ASS_SERVER_START;
      free_tcp (pwm);
      if (pwm->cancel)
        return GPG_ERR_CANCELED;
      return n == LIBSSH2_ERROR_TIMEOUT ? GPG_ERR_ETIMEDOUT : rc;
    }

  return _setup_ssh_shell (pwm);
}

static gpg_error_t
_setup_ssh_shell (pwm_t * pwm)
{
  int n;
  gpg_error_t rc;

  do
    {
      n = libssh2_channel_shell (pwm->tcp->ssh->channel);
      TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
    }
  while (n == LIBSSH2_ERROR_EAGAIN);

  if (n)
    {
      rc = GPG_ERR_ASS_SERVER_START;
      if (pwm->cancel)
        return GPG_ERR_CANCELED;
      return n == LIBSSH2_ERROR_TIMEOUT ? GPG_ERR_ETIMEDOUT : rc;
    }

  return ssh_connect_finalize (pwm);
}

static gpg_error_t
ssh_connect_finalize (pwm_t * pwm)
{
  libssh2_session_set_blocking (pwm->tcp->ssh->session, 1);
#ifdef __MINGW32__
  return assuan_socket_connect_fd (pwm->ctx, pwm->fh, 0);
#else
  return assuan_socket_connect_fd (pwm->ctx, pwm->fd, 0);
#endif
}

static gpg_error_t
_setup_ssh_init (pwm_t * pwm)
{
  int n;

  do
    {
      n = libssh2_session_handshake (pwm->tcp->ssh->session,
		      HANDLE2SOCKET (pwm->fd));
      TEST_TIMEOUT (pwm, pwm->tcp->ssh, n);
    }
  while (n == LIBSSH2_ERROR_EAGAIN);

  if (n)
    {
      if (pwm->cancel)
        return GPG_ERR_CANCELED;
      return n == LIBSSH2_ERROR_TIMEOUT ? GPG_ERR_ETIMEDOUT
        : GPG_ERR_ASSUAN_SERVER_FAULT;
    }

  return verify_hostkey (pwm);
}

gpg_error_t
_setup_ssh_session (pwm_t * pwm)
{
  gpg_error_t rc;

  if (!pwm->tcp->ssh->session)
    {
      pwm->tcp->ssh->session = libssh2_session_init_ex (ssh_malloc, ssh_free,
							ssh_realloc, NULL);
      if (!pwm->tcp->ssh->session)
	return GPG_ERR_ENOMEM;

      libssh2_session_flag (pwm->tcp->ssh->session, LIBSSH2_FLAG_COMPRESS, 1);
      libssh2_session_set_timeout (pwm->tcp->ssh->session,
				   pwm->socket_timeout * 1000);

      if (pwm->use_agent == 1)
	pwm->tcp->ssh->agent = libssh2_agent_init (pwm->tcp->ssh->session);
    }

  if (!pwm->tcp->ssh->session)
    return GPG_ERR_ENOMEM;

  libssh2_session_set_blocking (pwm->tcp->ssh->session, 0);
  rc = _setup_ssh_init (pwm);
  if (!rc)
    return rc;

  free_tcp (pwm);
  return rc;
}

gpg_error_t
_do_ssh_connect (pwm_t * pwm, const char *host, int port,
		 const char *identity, const char *user,
		 const char *known_hosts)
{
  gpg_error_t rc;

  if (!pwm)
    return GPG_ERR_INV_ARG;

  rc = init_ssh (pwm, host, port, identity, user, known_hosts,
                 pwm->use_agent == 1);
  if (rc)
    return rc;

  rc = tcp_connect_common (pwm);
  if (rc)
    goto fail;

  pwm->tcp->ssh->timeout = pwm->socket_timeout;
  rc = _setup_ssh_session (pwm);

fail:
  if (rc)
    free_tcp (pwm);

  return rc;
}

/*
 * ssh[46]://[username@][hostname][:port]
 *
 * Any missing parameters are checked for in init_ssh().
 */
gpg_error_t
_parse_ssh_url (const char *str, char **host, int *port, char **user)
{
  const char *p;
  int len;
  gpg_error_t rc;

  *host = *user = NULL;
  *port = 22;
  p = strrchr (str, '@');
  if (p)
    {
      len = strlen (str) - strlen (p) + 1;
      *user = pwmd_malloc (len);
      if (!*user)
	return gpg_error_from_errno (ENOMEM);

      snprintf (*user, len, "%s", str);
      p++;
    }
  else
    p = str;

  rc = parse_hostname_common (p, host, port);
  if (rc)
    pwmd_free (*user);

  return rc;
}
