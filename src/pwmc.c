/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <libpwmd.h>
#include <assuan.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libgen.h>
#include <ctype.h>
#include <time.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_STROPTS_H
#include <stropts.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifndef __MINGW32__
#ifdef HAVE_TERMIOS_H
#    include <termios.h>
#endif
#ifdef HAVE_ERR_H
#    include <err.h>
#endif
#include <sys/select.h>
#endif

#include "util-string.h"
#include "util-slist.h"

#ifdef WITH_GNUTLS
#include <gnutls/gnutls.h>
#endif

#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif

#ifdef HAVE_GETOPT_LONG
#    ifdef HAVE_GETOPT_H
#    include <getopt.h>
#    endif
#else
#    include "getopt_long.h"
#endif

#ifdef HAVE_LIBREADLINE
#if defined(HAVE_READLINE_READLINE_H)
#    include <readline/readline.h>
#elif defined(HAVE_READLINE_H)
#    include <readline.h>
#endif /* !defined(HAVE_READLINE_H) */
static int interactive_error;
static int interactive;
#endif /* HAVE_LIBREADLINE */

#ifdef HAVE_READLINE_HISTORY
#    if defined(HAVE_READLINE_HISTORY_H)
#    include <readline/history.h>
#elif defined(HAVE_HISTORY_H)
#    include <history.h>
#    endif
#endif /* HAVE_READLINE_HISTORY */

#ifndef LINE_MAX
#define LINE_MAX	2048
#endif

#include "gettext.h"
#define N_(msgid)	gettext(msgid)

#include "mem.h"
#include "version.h"

#define DEFAULT_STATUS_IGNORE   "KEEPALIVE,GPGME,PASSPHRASE_INFO,PASSPHRASE_HINT"
#define DEFAULT_PIN_TIMEOUT	30
#define DEFAULT_PIN_TRIES	3

#define FINISH(rc) (gpg_err_source(rc) == GPG_ERR_SOURCE_UNKNOWN) \
					? gpg_error(rc) : rc

enum
  {
    SAVE_WHICH_SAVE,
    SAVE_WHICH_PASSWD,
    SAVE_WHICH_GENKEY
  };

static int no_pinentry;
static pwm_t *pwm;
static char *filename;
static int save;
static char *keyid;
static char *sign_keyid;
static int symmetric;
static char *keyparams;
static char *keyfile;
static char *new_keyfile;
static char *sign_keyfile;
static int tries;
static int local_pin;
static int inquirefd;
static int statusfd;
FILE *statusfp;
static int quiet;
static char **status_ignore;
static int skip_pinentry_status;

struct inquire_s
{
  int fd;
  char *line;
  size_t len;
  size_t size;			// from stat(2).
  char *last_keyword;
};

struct userid_s {
    char *userid;
    char *name;
    char *email;
    char *comment;
};

struct keyid_s {
    char *keyid;
    char *fpr;
    char *grip;
    char *card;
    int can_sign;
    int can_encrypt;
    int can_certify;
    int can_auth;
    int expired;
    time_t expires;
    time_t created;
    int secret;
    int revoked;
    int algo;
    unsigned bits;
    char *curve;
    struct slist_s *userids;
    struct slist_s *subkeys;
};

static gpg_error_t finalize ();
static gpg_error_t set_inquire (int fd, const char *line,
				struct inquire_s **result);
static gpg_error_t parse_dotcommand (const char *line, char **result,
				     size_t * len, struct inquire_s *inq);
static gpg_error_t open_command (char *line);
#ifndef HAVE_STRSEP
char *strsep (char **, const char *);
#endif
#if !defined (HAVE_ERR) && !defined (HAVE_ERR_H)
int err (int, const char *, ...);
#endif

static void
show_error (pwm_t *h, gpg_error_t rc, const char *str)
{
#ifdef WITH_GNUTLS
  const char *tlsstr;
  int e = pwmd_gnutls_error (h, &tlsstr);

  if (e)
    fprintf(stderr, "TLS: %s\n", tlsstr);
#else
  (void)h;
#endif
  fprintf (stderr, "ERR %i: %s%s%s%s", rc, gpg_strerror (rc),
	   str ? ": " : "", str ? str : "", str ? "" : "\n");
}

static void
reset_keyfiles ()
{
  pwmd_free (keyfile);
  pwmd_free (new_keyfile);
  pwmd_free (sign_keyfile);
  keyfile = new_keyfile = sign_keyfile = NULL;
}

static void
usage (const char *pn, int status)
{
  fprintf (status == EXIT_FAILURE ? stderr : stdout,
	   N_("Usage: %s [options] [file]\n"
	      "   --socket <string>\n"
	      "       a url string to connect to (%s, see below)\n"
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
	      "   --connect-timeout <seconds>\n"
	      "       seconds before connecting to a remote host fails (0=disabled, 120)\n"
	      "   --socket-timeout <seconds>\n"
	      "       seconds before a remote command fails (0=disabled, 300)\n"
#endif
#ifdef WITH_GNUTLS
	      "   --ca-cert <filename>\n"
	      "       certificate authority (CA) used to sign the server cert\n"
	      "   --client-cert <filename>\n"
	      "       client certificate to use for authentication\n"
	      "   --client-key <filename>\n"
	      "       key file used to protect the client certificate\n"
	      "   --tls-priority <string>\n"
	      "       compression, cipher and hash algorithm string\n"
              "       (SECURE256:SECURE192:SECURE128:-VERS-SSL3.0:-VERS-TLS1.0)\n"
	      "   --no-tls-verify\n"
	      "       disable verifying the hostname against the server certificate\n"
	      "   --tls-fingerprint <string>\n"
	      "       a SHA-256 hash of the server fingerprint to verify against\n"
#endif
#ifdef WITH_SSH
	      "   --no-ssh-agent\n"
	      "       disable SSH agent use (enabled when SSH_AUTH_SOCK is set)\n"
	      "   --identity, -i <filename>\n"
	      "       the ssh identity file to use for authentication\n"
	      "   --knownhosts, -k <filename>\n"
	      "       the ssh knownhosts file to use (~/.ssh/knownhosts)\n"
              "   --ssh-passphrase-file <filename>\n"
              "       read the SSH private key passphrase from filename\n"
#endif
	      "   --no-lock\n"
	      "       do not lock the data file upon opening it\n"
	      "   --lock-timeout <N>\n"
	      "       time in tenths of a second to wait for a locked data file (50)\n"
	      "   --name, -n <string>\n"
	      "       set the client name\n"
	      "   --local-pinentry\n"
	      "       force using a local pinentry\n"
	      "   --no-pinentry\n"
	      "       disable pinentry both remotely and locally\n"
	      "   --ttyname, -y <path>\n"
	      "       tty that pinentry will use\n"
	      "   --ttytype, -t <string>\n"
	      "       pinentry terminal type (default is $TERM)\n"
	      "   --display, -d\n"
	      "       pinentry display (default is $DISPLAY)\n"
	      "   --lc-ctype <string>\n"
	      "       locale setting for pinentry\n"
	      "   --lc-messages <string>\n"
	      "       locale setting for pinentry\n"
	      "   --tries <N>\n"
	      "       number of pinentry tries before failing (3)\n"
	      "   --timeout <seconds>\n"
	      "       pinentry timeout\n"
	      "   --inquire <COMMAND>\n"
	      "       the specified command (with any options) uses a server inquire while\n"
	      "       command data is read via the inquire file descriptor (stdin)\n"
	      "   --inquire-line, -L <STRING>\n"
	      "       the initial line to send (i.e., element path) before the inquire data\n"
	      "   --inquire-fd <FD>\n"
	      "       read inquire data from the specified file descriptor (stdin)\n"
	      "   --inquire-file <filename>\n"
	      "       read inquire data from the specified filename\n"
	      "   --output-fd <FD>\n"
	      "       redirect command output to the specified file descriptor\n"
	      "   --save, -S\n"
	      "       send the SAVE command before exiting\n"
	      "   --passphrase-file <filename>\n"
	      "       obtain the passphrase from the specified filename\n"
	      "   --new-passphrase-file <filename>\n"
	      "       obtain the passphrase to save with from the specified filename\n"
	      "   --sign-passphrase-file <filename>\n"
	      "       obtain the passphrase to sign with from the specified filename\n"
	      "   --key-params <filename>\n"
	      "       key parameters to use for key generation (pwmd default)\n"
	      "   --keyid <recipient>[,<recipient>]\n"
	      "       the public key ID to u\n"
	      "   --sign-keyid <string>\n"
	      "       the key ID to sign the data file with\n"
              "   --symmetric\n"
              "       use conventional encryption with optional signer(s) for new files\n"
	      "   --no-status\n"
	      "       disable showing of status messages from the server\n"
              "   --status-state\n"
              "       enable receiving of client STATE status messages\n"
              "   --status-fd <FD>\n"
              "       redirect status messages to the specified file descriptor\n"
              "   --status-ignore <string[,...]>\n"
              "       prevent parsing of the specified status message keywords\n"
	      "   --quiet\n"
	      "       disable showing of extra messages (implies --no-status)\n"
#ifdef HAVE_LIBREADLINE
	      "   --no-interactive\n"
	      "       disable interactive mode\n"
#endif
	      "   --version\n"
	      "   --help\n"),
           pn,
#ifdef DEFAULT_PWMD_SOCKET
	   DEFAULT_PWMD_SOCKET
#else
	   "~/.pwmd/socket"
#endif
	   );
  fprintf (status == EXIT_FAILURE ? stderr : stdout,
	   N_("\n"
	      "An optional url may be in the form of:\n"
	      "    --socket /path/to/socket\n"
	      "    --socket file://[/path/to/socket]\n"
#ifdef WITH_SSH
	      "    or\n"
	      "    --socket ssh[46]://[username@]hostname[:port] (uses ssh-agent)\n"
	      "    -i identity_file --socket ssh[46]://[username@]hostname[:port]\n"
#endif
#ifdef WITH_GNUTLS
	      "    or\n"
	      "    --socket tls[46]://hostname[:port] --ca-cert filename --client-cert filename\n"
	      "        --client-key filename\n"
#endif
#ifdef HAVE_LIBREADLINE
	      "\n"
	      "Interactive mode is used when input is from a terminal.\n"
#endif
	      ));
  exit (status);
}

static gpg_error_t
inquire_cb (void *user, const char *keyword, gpg_error_t rc,
	    char **data, size_t * size)
{
  struct inquire_s *inq = user;
  int is_password = 0;
  int is_newpassword = 0;
  int sign = 0;
  int is_keyparam = 0;

  *data = NULL;
  *size = 0;

  if (rc)
    return rc;

  if (!strcmp (keyword, "PASSPHRASE"))
    is_password = 1;
  else if (!strcmp (keyword, "SIGN_PASSPHRASE"))
    sign = 1;
  else if (!strcmp (keyword, "NEW_PASSPHRASE"))
    is_newpassword = 1;
#ifdef HAVE_LIBREADLINE
  else if (!strcmp (keyword, "KEYPARAM") && !interactive)
    {
#else
  else if (!strcmp (keyword, "KEYPARAM"))
    {
#endif
      int fd;
      if (!keyparams || !*keyparams)
	return gpg_error (GPG_ERR_INV_PARAMETER);

      fd = open (keyparams, O_RDONLY);
      if (fd == -1)
	{
          fprintf (stderr, "%s: %s\n", keyparams, strerror (errno));
	  return gpg_error_from_syserror ();
        }

      rc = set_inquire (fd, NULL, &inq);
      if (rc)
	{
	  close (fd);
	  return rc;
	}

      if (!quiet)
	fprintf (stderr, N_("Using file '%s' as %s.\n"), keyparams, keyword);

      is_keyparam = 1;
    }

  if ((is_password && !keyfile) || (is_newpassword && !new_keyfile)
           || (sign && !sign_keyfile))
    {
      char *tmp;
      int local;

      /* Try to use the local pinentry between inquires (new/sign/passphrase).
       * If --no-pinentry was specified then the passphrase is read from the
       * terminal as usual. */
      pwmd_getopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, &local);
      pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);
      rc = pwmd_password (pwm, keyword, &tmp, &inq->len);
      pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, local);
      if (rc && gpg_err_code (rc) != GPG_ERR_EOF)
	return rc;

      pwmd_free (inq->line);
      inq->line = tmp;
      *data = inq->line;
      *size = inq->len;
      rc = gpg_error (GPG_ERR_EOF);
      goto done;
    }
  else if ((is_newpassword && new_keyfile) || (is_password && keyfile)
      || (sign && sign_keyfile))
    {
      int fd;

      if (sign)
        fd = open (sign_keyfile, O_RDONLY);
      else
        fd = open (is_password || sign ? keyfile : new_keyfile, O_RDONLY);

      if (fd == -1)
	{
          if (sign)
            fprintf (stderr, "%s: %s\n", sign_keyfile, strerror (errno));
          else
            fprintf (stderr, "%s: %s\n", is_newpassword ? new_keyfile
                     : keyfile, strerror (errno));
	  return gpg_error_from_syserror ();
	}

      rc = set_inquire (fd, NULL, &inq);
      if (rc)
	{
	  close (fd);
	  return rc;
	}

      if (!quiet)
	fprintf (stderr, N_("Using keyfile '%s' as %s.\n"),
		 sign ? sign_keyfile : is_newpassword ? new_keyfile
                 : keyfile, keyword);
    }
#ifdef HAVE_LIBREADLINE
  else if ((!inq->last_keyword || strcmp (keyword, inq->last_keyword))
           && interactive && inq->fd == STDIN_FILENO)
    {
      fprintf (stderr,
	       N_
	       ("%sPress CTRL-D to send the current line. Press twice to end. %s:\n"),
                inq->last_keyword ? "\n" : "", keyword);
      pwmd_free (inq->last_keyword);
      inq->last_keyword = pwmd_strdup (keyword);
    }
#endif

  /* The first part of the command data. */
  if (inq->len)
    {
      *data = inq->line;
      *size = inq->len;
      inq->len = 0;
      rc = inq->fd == -1 ? gpg_error (GPG_ERR_EOF) : 0;
      goto done;
    }

  *size = read (inq->fd, inq->line, ASSUAN_LINELENGTH);
  if (*size == -1)
    {
      *size = 0;
      return gpg_error (gpg_error_from_syserror ());
    }
  else if (*size)
    *data = inq->line;
  else if (inq->fd != STDIN_FILENO &&
           (is_newpassword || is_password || sign || is_keyparam))
    {
      *inq->line = 0;
      inq->size = 1;
      *data = inq->line;
      *size = 1;
    }

  if (((is_newpassword && new_keyfile) || (is_password && keyfile)
       || (sign && sign_keyfile) || (keyparams && is_keyparam))
      && *size == inq->size)
    rc = gpg_error (GPG_ERR_EOF);

  if (!rc)
    rc = *size ? 0 : gpg_error (GPG_ERR_EOF);

done:
  if (gpg_err_code (rc) == GPG_ERR_EOF)
    {
      pwmd_free (inq->last_keyword);
      inq->last_keyword = NULL;
    }

  return rc;
}

static int
status_msg_cb (void *data, const char *line)
{
  char *p = strchr (line, ' ');
  char **s;

  (void)data;

  /* Ignore status messages specified by the client via --status-ignore. */
  for (s = status_ignore; s && *s; s++)
    {
      char *tmp = strchr (line, ' ');
      size_t len = tmp ? strlen (line) - strlen (tmp) : strlen (line);

      if (!strncmp (line, *s, len) && len == strlen (*s))
        return 0;
    }

#ifdef HAVE_LIBREADLINE
  if (interactive && !strncmp (line, "XFER ", 5)
#else
  if (!strncmp (line, "XFER ", 5)
#endif
      && *line != '#' && p && strchr (p, ' ') && *++p)
    {
      char *p1 = strchr (p, ' ');
      int a = strtol (p, NULL, 10);

      if (isdigit (*p) && p1)
	{
	  int b = strtol (p1, NULL, 10);
	  int t = a && b ? a * 100 / b : 0;

	  fprintf (statusfp, "\rS:XFER %i/%i %i%%%s", a, b, t,
                   a == b ? "\n" : "");
	  fflush (statusfp);
	  return 0;
	}
    }
#ifdef HAVE_LIBREADLINE
  else if (interactive && (!strncmp (line, "DECRYPT", 7)
                           || !strncmp (line, "ENCRYPT", 7)
                           || !strncmp (line, "GENKEY", 6)))
#else
  else if (!strncmp (line, "DECRYPT", 7) || !strncmp (line, "ENCRYPT", 7)
            || !strncmp (line, "GENKEY", 6))
#endif
    {
      if (skip_pinentry_status)
        goto done;
    }

  fprintf (statusfp, "S:%s\n", line);
  fflush (statusfp);
done:
#ifdef HAVE_LIBREADLINE
  rl_on_new_line ();
#endif
  return 0;
}

static gpg_error_t
process_cmd ()
{
  return pwmd_process (pwm);
}

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
static pwmd_socket_t
is_remote_url (const char *str)
{
  if (!str)
    return PWMD_SOCKET_LOCAL;

#ifdef WITH_SSH
  if (strstr (str, "ssh://") || strstr (str, "ssh4://")
      || strstr (str, "ssh6://"))
    return PWMD_SOCKET_SSH;
#endif

#ifdef WITH_GNUTLS
  if (strstr (str, "tls://") || strstr (str, "tls4://")
      || strstr (str, "tls6://"))
    return PWMD_SOCKET_TLS;
#endif

  return PWMD_SOCKET_LOCAL;
}
#endif

static char *
escape (const char *str)
{
  const char *p;
  char *buf = pwmd_malloc (ASSUAN_LINELENGTH + 1), *b = buf;
  size_t len = 0;

  for (p = str; *p; p++, len++)
    {
      if (len == ASSUAN_LINELENGTH)
	break;

      if (*p == '\\')
	{
	  switch (*++p)
	    {
	    case 't':
	      *b++ = '\t';
	      break;
	    case 'n':
	      *b++ = '\n';
	      break;
	    case 'v':
	      *b++ = '\v';
	      break;
	    case 'b':
	      *b++ = '\b';
	      break;
	    case 'f':
	      *b++ = '\f';
	      break;
	    case 'r':
	      *b++ = '\r';
	      break;
	    default:
	      *b++ = *p;
	      break;
	    }

	  if (!*p)
	    break;

	  continue;
	}

      *b++ = *p;
    }

  *b = 0;
  return buf;
}

static void
free_inquire (struct inquire_s *inq)
{
  if (!inq)
    return;

  pwmd_free (inq->line);

  if (inq->fd != -1 && inq->fd != STDIN_FILENO)
    close (inq->fd);

  pwmd_free (inq->last_keyword);
  pwmd_free (inq);
}

/* When *result is not NULL it is updated to the new values and not
 * reallocated. */
static gpg_error_t
set_inquire (int fd, const char *line, struct inquire_s **result)
{
  struct inquire_s inq = { 0 };
  struct stat st = { 0 };
  gpg_error_t rc;

  if (fd != -1)
    {
      if (fstat (fd, &st) == -1)
	return gpg_error_from_syserror ();

      inq.size = st.st_size;
    }

  inq.fd = fd;
  inq.line = pwmd_calloc (1, ASSUAN_LINELENGTH);
  if (!inq.line)
    return GPG_ERR_ENOMEM;

  if (line)
    {
      char *s = escape (line);

      if (!s)
	{
	  pwmd_free (inq.line);
	  return GPG_ERR_ENOMEM;
	}

      if (strlen (s) >= ASSUAN_LINELENGTH)
	{
	  pwmd_free (inq.line);
	  pwmd_free (s);
	  return gpg_error (GPG_ERR_LINE_TOO_LONG);
	}

      strncpy (inq.line, s, ASSUAN_LINELENGTH - 1);
      inq.len = strlen (s);
      pwmd_free (s);
    }

  rc = pwmd_setopt (pwm, PWMD_OPTION_INQUIRE_TOTAL,
		    st.st_size ? st.st_size + strlen (inq.line) : 0);
  if (rc)
    {
      pwmd_free (inq.line);
      return rc;
    }

  if (*result == NULL)
    *result = pwmd_malloc (sizeof (struct inquire_s));
  else
    {
      if ((*result)->fd != -1 && (*result)->fd != STDIN_FILENO)
	close ((*result)->fd);

      pwmd_free ((*result)->line);
      (*result)->line = NULL;
      (*result)->fd = -1;
      (*result)->len = 0;
    }

  memcpy (*result, &inq, sizeof (struct inquire_s));
  memset (&inq, 0, sizeof (struct inquire_s));
  return rc;
}

#ifdef HAVE_LIBREADLINE
static int
interactive_hook (void)
{
  interactive_error = process_cmd ();

  if (interactive_error)
    rl_event_hook = NULL;

  return 0;
}

static int
get_readline_char (FILE *fp)
{
  if (rl_line_buffer
      && (!strncmp (rl_line_buffer, ".set passphrase-file ", 16)
          || !strncmp (rl_line_buffer, ".set new-passphrase-file ", 20)
          || !strncmp (rl_line_buffer, ".set sign-passphrase-file ", 21)))
    rl_inhibit_completion = 0;
  else if (rl_line_buffer
           && !strncmp (rl_line_buffer, ".redir ", 7))
    {
      char *p = strchr (rl_line_buffer, ' ');

      if (p && strchr (++p, ' '))
        rl_inhibit_completion = 1;
      else
        rl_inhibit_completion = 0;
    }
  else if (rl_line_buffer
           && !strncmp (rl_line_buffer, ".read ", 6))
    {
      char *p = rl_line_buffer + 6;

      if (strstr (p, "--prefix "))
        {
          p = strstr (p, "--prefix ");
          p += 9;
          p = strchr (p, ' ');
          if (p)
            p++;
        }

      if (!p || strchr (p, ' '))
        rl_inhibit_completion = 1;
      else
        rl_inhibit_completion = 0;
    }
  else
    rl_inhibit_completion = 1;

  return fgetc (fp);
}

static void
print_help ()
{
  fprintf (stderr,
	   N_
	   ("------------------------------------------------------------\n"
	    "Elements are TAB delimited. Type HELP for protocol commands.\n"
            "Type .help for pwmc commands. Press CTRL-D to quit.\n"
	    "------------------------------------------------------------\n"));
}
#endif

static char *
parse_arg (const char *src, char *dst, size_t len)
{
  char *p = dst;
  const char *s = src;
  size_t n = 0;

  for (; s && *s && *s != ' ' && n < len; s++, n++)
    *p++ = *s;

  *p = 0;
  return dst;
}

static char *
parse_opt (char **line, const char *opt, gpg_error_t * rc)
{
  static char result[ASSUAN_LINELENGTH] = { 0 }, *r = result;
  char *s = strstr (*line, opt);

  *rc = 0;
  result[0] = 0;
  r = result;

  if (s)
    {
      size_t len = 0;
      int quote = 0;
      size_t rlen = strlen (opt);
      char *p = s + rlen;
      int lastc = 0;

      while (*p && *p == ' ')
	{
	  rlen++;
	  p++;
	}

      for (; *p && len < sizeof (result) - 1; p++, rlen++)
	{
	  if (isspace (*p) && !quote)
	    break;

	  if (*p == '\"' && lastc != '\\')
	    {
	      quote = !quote;
	      lastc = *p;
	      continue;
	    }

	  *r++ = lastc = *p;
	  len++;
	}

      *r = 0;

      if (len >= sizeof (result) - 1)
	*rc = GPG_ERR_LINE_TOO_LONG;
      else
	{
	  p = s + rlen;

	  while (*p && *p == ' ')
	    p++;

	  *line = p;
	}
    }

  return result;
}

static gpg_error_t
read_command (const char *line, char **result, size_t * len)
{
  int fd;
  gpg_error_t rc = 0;
  char *file = NULL;
  struct inquire_s *inq = NULL;
  char *p = (char *) line;
  const char *prefix = parse_opt (&p, "--prefix", &rc);
  char filebuf[ASSUAN_LINELENGTH];

  if (rc)
    return rc;

  rc = GPG_ERR_SYNTAX;

  if (p && *p)
    {
      while (*p && isspace (*p))
	p++;

      file = parse_arg (p, filebuf, sizeof (filebuf));
      if (file && *file)
	{
	  p += strlen (file) + 1;

	  while (*p && isspace (*p))
	    p++;

	  if (*p)
	    rc = 0;
	}
    }

  if (rc)
    {
      fprintf (stderr,
	       N_
	       ("Usage: .read [--prefix <string>] <filename> <command> [args]\n"));
      fprintf (stderr,
	       N_
	       ("Use '\\' to escape special characters in the --prefix (\\t = TAB, \\\\ = \\)\n"));
      return rc;
    }

  fd = open (file, O_RDONLY);
  if (fd == -1)
    return gpg_error_from_syserror ();

  rc = set_inquire (fd, prefix && *prefix ? prefix : NULL, &inq);
  if (rc)
    {
      close (fd);
      return rc;
    }

  rc = pwmd_command (pwm, result, len, inquire_cb, inq, "%s", p);
  free_inquire (inq);
  return rc;
}

static gpg_error_t
redir_command (const char *line)
{
  const char *p = line;
  int fd;
  gpg_error_t rc = GPG_ERR_SYNTAX;
  char *file = NULL;
  struct inquire_s *inq = NULL;
  char *result = NULL;
  size_t len = 0;
  char filebuf[ASSUAN_LINELENGTH];

  if (p && *p && *++p)
    {
      file = parse_arg (p, filebuf, sizeof (filebuf));
      if (file && *file)
	{
	  p += strlen (file) + 1;

	  while (*p && isspace (*p))
	    p++;

	  if (*p)
	    rc = 0;
	}
    }

  if (rc)
    {
      fprintf (stderr, N_("Usage: .redir <filename> <command> [args]\n"));
      return rc;
    }

#ifdef __MINGW32__
  fd = open (file, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0600);
#else
  fd = open (file, O_WRONLY | O_CREAT | O_TRUNC, 0600);
#endif
  if (fd == -1)
    return gpg_error_from_syserror ();

#ifdef HAVE_LIBREADLINE
  rc = set_inquire (interactive ? STDIN_FILENO : inquirefd, NULL, &inq);
#else
  rc = set_inquire (inquirefd, NULL, &inq);
#endif
  if (rc)
    {
      close (fd);
      return rc;
    }

  rc = parse_dotcommand (p, &result, &len, inq);
  if (!rc && result && len--)
    {				// null byte which is always appended
      if (write (fd, result, len) != len)
	rc = GPG_ERR_TOO_SHORT;
      pwmd_free (result);
    }

  free_inquire (inq);
  close (fd);
  return rc;
}

static gpg_error_t
help_command (const char *line)
{
  (void)line;
  fprintf (stderr,
	   N_("Type HELP for protocol commands. Available pwmc commands:\n\n"
	      "  .redir <filename> <command>\n"
	      "      redirect the output of a command to the specified file\n"
	      "\n"
	      "  .open <filename>\n"
	      "      open the specified filename losing any changes to the current one\n"
	      "\n"
	      "  .read [--prefix <string>] <filename> <command> [args]\n"
	      "      obtain data from the specified filename for an inquire command\n"
	      "\n"
	      "  .set help | <name> [<value>]\n"
	      "      set option <name> to <value>\n"
	      "\n"
	      "  .genkey [args]\n"
	      "      generate a new key\n"
	      "\n"
	      "  .save [args]\n"
	      "      write changes of the file to disk\n"
	      "\n"
	      "  .passwd [args]\n"
	      "      change the passphrase of a data file\n"
              "\n"
              "  .listkeys [--options] [pattern[,..]]\n"
              "      show human readable output of the LISTKEYS command\n"
	      "\n"
	      "  .help\n"
	      "      this help text\n"));
  return 0;
}

static gpg_error_t
open_command (char *line)
{
  struct inquire_s *inq = NULL;
  char *file = line, *tmp = NULL;
  gpg_error_t rc;
  int local;

  while (file && isspace (*file))
    file++;

  if (file && *file)
    {
      char *p = strrchr (file, ' ');

      if (p)
        {
          while (isspace (*p))
            p++;

          if (*p)
            {
              file = tmp = pwmd_strdup (file + (strlen (file)-strlen (p)));
              if (!file || !*file)
                {
                  pwmd_free (file);
                  file = tmp = NULL;
                }
            }
          else
            file = NULL;
        }
    }

  if (!file || !*file)
    {
      fprintf (stderr, N_("Usage: .open [--options] <filename>\n"));
      return GPG_ERR_SYNTAX;
    }

#ifdef HAVE_LIBREADLINE
  if (interactive || !quiet)
#else
  if (!quiet)
#endif
    fprintf (stderr, N_("Opening data file \"%s\" ...\n"), file);

#ifdef HAVE_LIBREADLINE
  rc = set_inquire (interactive ? STDIN_FILENO : -1, NULL, &inq);
#else
  rc = set_inquire (-1, NULL, &inq);
#endif
  if (rc)
    {
      if (tmp)
        pwmd_free (file);
      return rc;
    }

  pwmd_getopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, &local);

  if (keyfile)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
      if (!rc)
        rc = pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);
    }
  else
    rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 0);

  if (!rc)
    rc = pwmd_open (pwm, file, inquire_cb, inq);

  pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, local);

#ifdef HAVE_LIBREADLINE
  if (interactive)
    reset_keyfiles ();
#endif

  free_inquire (inq);
  if (!rc)
    {
      if (tmp)
        {
          pwmd_free (filename);
          filename = tmp;
        }
      else
        {
          char *p = pwmd_strdup (file);

          pwmd_free (filename);
          filename = p;
          if (!filename)
            rc = GPG_ERR_ENOMEM;
        }
    }
  else
    {
      pwmd_free (tmp);
      pwmd_free (filename);
      filename = NULL;
    }

  return rc;
}

static gpg_error_t
set_command (const char *line)
{
  gpg_error_t rc = 0;
  char name[256] = { 0 };
  char value[512] = { 0 };
  char *namep;
  char *valuep;
  const char *p = line;

  while (p && *p && isspace (*p))
    p++;

  namep = parse_arg (p, name, sizeof (name));
  if (!namep || !*namep)
    {
      fprintf (stderr, N_("Usage: .set help | <name> [<value>]\n"));
      return GPG_ERR_SYNTAX;
    }

  p += strlen (namep);
  while (p && *p && isspace (*p))
    p++;

  valuep = parse_arg (p, value, sizeof (value));

  if (!strcmp (name, "passphrase-file") || !strcmp (name, "new-passphrase-file")
      || !strcmp (name, "sign-passphrase-file"))
    {
      int is_newkeyfile = 1;
      int sign = !strcmp (name, "sign-passphrase-file");

      if (!strcmp (name, "passphrase-file") || sign)
	is_newkeyfile = 0;

      if (is_newkeyfile)
	{
	  pwmd_free (new_keyfile);
	  new_keyfile = NULL;
	}
      else if (sign)
        {
          pwmd_free (sign_keyfile);
	  sign_keyfile = NULL;
        }
      else
	{
	  pwmd_free (keyfile);
	  keyfile = NULL;
	}

      if (!rc && *valuep)
	{
	  if (is_newkeyfile)
	    new_keyfile = pwmd_strdup (value);
	  else if (sign)
            sign_keyfile = pwmd_strdup (value);
          else
	    keyfile = pwmd_strdup (value);

          rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
	}
      else if (!local_pin && !no_pinentry)
	{
	  pwmd_socket_t t;

	  pwmd_socket_type (pwm, &t);
	  if (t == PWMD_SOCKET_LOCAL)
            rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 0);
	}
    }
  else if (!strcmp(name, "pinentry-timeout"))
    {
      char *e = NULL;
      int n = strtol(valuep, &e, 10);

      if (e && *e)
        return gpg_error (GPG_ERR_INV_VALUE);

      if (!*valuep)
        n = DEFAULT_PIN_TIMEOUT;

      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TIMEOUT, n);
    }
  else if (!strcmp (name, "help"))
    {
      fprintf (stderr,
	       N_
	       ("Set a libpwmd or pwmc option. The option name and optional value is space\n"
		"delimited. When no value is specified the option is unset.\n\n"
		"passphrase-file [<filename>]\n"
		"    set or unset the file to be used when a passphrase is required (*)\n"
		"\n"
		"new-passphrase-file [<filename>]\n"
		"    set or unset the file to be used when a new passphrase is required (*)\n"
		"\n"
		"sign-passphrase-file [<filename>]\n"
		"    set or unset the file to be used when a passphrase is required for\n"
                "    signing (symmetric) (*)\n"
		"\n"
                "pinentry-timeout <seconds>\n"
                "    the amount of seconds before pinentry gives up waiting for input\n"
		"\n"
		"* = the next protocol command will unset this value\n"
		));
    }
  else
    rc = GPG_ERR_UNKNOWN_OPTION;

  return rc;
}

static gpg_error_t
do_save_passwd_command (const char *line, int which)
{
  struct inquire_s *inq = NULL;
  gpg_error_t rc;
  int local;

#ifdef HAVE_LIBREADLINE
  rc = set_inquire (interactive ? STDIN_FILENO : -1, NULL, &inq);
#else
  rc = set_inquire (-1, NULL, &inq);
#endif
  if (rc)
    return rc;

  pwmd_getopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, &local);

  if (new_keyfile || keyfile || sign_keyfile)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
      if (!rc)
        pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);
    }
  else
    rc = pwmd_setopt (pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 0);

  if (!rc)
    {
      skip_pinentry_status = 1;

      if (which == SAVE_WHICH_SAVE)
        rc = pwmd_save (pwm, line, inquire_cb, inq);
      else if (which == SAVE_WHICH_PASSWD)
        rc = pwmd_passwd (pwm, line, inquire_cb, inq);
      else
        rc = pwmd_genkey (pwm, line, inquire_cb, inq);

      skip_pinentry_status = 0;
    }

  pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, local);

#ifdef HAVE_LIBREADLINE
  if (interactive)
    reset_keyfiles ();
#endif

  free_inquire (inq);
  return rc;
}

static gpg_error_t
save_command (const char *line)
{
  return do_save_passwd_command (line, SAVE_WHICH_SAVE);
}

static void
search_and_replace (char *str, const char *s, const char r)
{
  char *p;

  p = strstr (str, s);
  if (p)
    {
      *p = r;
      while (*++p)
        *p = *(p+1);
      search_and_replace (str, s, r);
      return;
    }
}

static char *
openpgp_unescape (char *str)
{
  search_and_replace (str, "\\x3a", ':');
  search_and_replace (str, "\\x5c", '\\');
  return str;
}

static void
free_listkeys (struct slist_s *keys)
{
  unsigned i, t;

  t = slist_length (keys);
  for (i = 0; i < t; i++)
    {
      struct keyid_s *key = slist_nth_data (keys, i);
      unsigned n, nt;

      nt = slist_length (key->userids);
      for (n = 0; n < nt; n++)
        {
          struct userid_s *userid = slist_nth_data (key->userids, n);

          pwmd_free (userid->userid);
          pwmd_free (userid->name);
          pwmd_free (userid->email);
          pwmd_free (userid->comment);
          pwmd_free (userid);
        }

      slist_free (key->userids);
      nt = slist_length (key->subkeys);
      for (n = 0; n < nt; n++)
        {
          struct keyid_s *sub = slist_nth_data (key->subkeys, n);

          pwmd_free (sub->keyid);
          pwmd_free (sub->fpr);
          pwmd_free (sub->grip);
          pwmd_free (sub->card);
          pwmd_free (sub->curve);
          pwmd_free (sub);
        }

      slist_free (key->subkeys);
      pwmd_free (key);
    }

  slist_free (keys);
}

static const char *
openpgp_algorithm (struct keyid_s *key)
{
  switch (key->algo)
    {
    case 1:
    case 2:
    case 3:
      return "RSA";
    case 8:
      return "KYBER";
    case 16:
    case 20:
      return "ELG";
    case 17:
      return "DSA";
    case 301:
    case 302:
    case 18:
      return "ECDH";
    case 303:
      return "EDDSA";
    default:
      break;
    }

  return N_("Unknown");
}

static void
display_listkeys (struct slist_s *keys)
{
  unsigned i, t;

  t = slist_length (keys);
  for (i = 0; i < t; i++)
    {
      struct keyid_s *key = slist_nth_data (keys, i);
      unsigned st = slist_length (key->subkeys);
      unsigned s;
      unsigned ut = slist_length (key->userids);
      unsigned u;

      for (u = 0; u < ut; u++)
        {
          struct userid_s *userid = slist_nth_data (key->userids, u);

          fprintf(stdout, N_("User ID: %s\n"), userid->userid);
        }

      for (s = 0; s < st; s++)
        {
          struct keyid_s *sub = slist_nth_data (key->subkeys, s);
          struct tm *tmp;
          const char *caps = NULL;
          char expires[11] = { 0 };
          char created[11] = { 0 };

          if (sub->expires)
            {
              tmp = localtime (&sub->expires);
              strftime (expires, sizeof (expires), "%Y-%m-%d", tmp);
            }

          tmp = localtime (&sub->created);
          strftime (created, sizeof (created), "%Y-%m-%d", tmp);

          if (sub->can_encrypt)
            caps = "E";
          else if (sub->can_sign)
            caps = "S";
          else if (sub->can_auth)
            caps = "A";
          else if (sub->can_certify)
            caps = "C";

          fprintf(stdout, N_(
                  "  Subkey %u: %s  [%s]%s%s  %s-%u%s%s\n"
                  "    Created: %s  %s%s\n"
                  "    Fingerprint: %s\n"
                  "    Keygrip: %s\n"
                  "%s%s%s"),
                  s+1,
                  sub->keyid,
                  caps,
                  sub->secret || sub->card ? "[P]" : "",
                  sub->revoked ? "[R]" : "",
                  openpgp_algorithm (sub),
                  sub->bits,
                  sub->curve ? "-" : "",
                  sub->curve ? sub->curve  : "",
                  created,
                  sub->expired ? N_("Expired: ") : expires[0] ? N_("Expires: ") : "",
                  sub->expired || expires[0] ? expires : "",
                  sub->fpr,
                  sub->grip,
                  sub->card ? N_("    Card: ") : "",
                  sub->card ? sub->card : "",
                  sub->card ? "\n" : "");
        }

      if (i+1 < t)
        fprintf (stdout, "\n");
    }
}

static gpg_error_t
listkeys_command (const char *pattern)
{
  gpg_error_t rc;
  char *result;
  char **lines = NULL, **p;
  struct slist_s *keys = NULL;

  rc = pwmd_command (pwm, &result, NULL, NULL, NULL, "LISTKEYS %s", pattern);
  if (rc)
    return rc;

  lines = str_split (result, "\n", 0);
  pwmd_free (result);
  if (!lines)
    return GPG_ERR_ENOMEM;

  for (p = lines; *p; p++)
    {
      struct keyid_s *key;
      char **fields = str_split (*p, ":", 0);
      int i, f;
      unsigned s;
      unsigned n_subkeys = 0;
      struct slist_s *tlist;
      
      if (!fields)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      key = pwmd_calloc (1, sizeof (struct keyid_s));
      if (!key)
        {
          strv_free (fields);
          rc = GPG_ERR_ENOMEM;
          break;
        }

      for (f = i = 0; i < 17; i++, f++)
        {
          int b = 0;

          if (i < 10)
            b = atoi (fields[f]) != 0;

          switch (i)
            {
            case 0: key->revoked = b; break;
            case 1: key->expired = b; break;
            case 4: key->can_encrypt = b; break;
            case 5: key->can_sign = b; break;
            case 7: key->secret = b; break;
            case 16: n_subkeys = strtoul (fields[f], NULL, 10); break;
            default:
              break;
            }
        }

      for (s = 0; s < n_subkeys; s++)
        {
          struct keyid_s *sub;
          int b = 0;

          sub = pwmd_calloc (1, sizeof (struct keyid_s));
          if (!sub)
            {
              strv_free (fields);
              rc = GPG_ERR_ENOMEM;
              break;
            }

          for (i = 0; i < 20; i++, f++)
            {
              if (i < 11)
                b = atoi (fields[f]) != 0;

              switch (i)
                {
                case 0: sub->revoked = b; break;
                case 1: sub->expired = b; break;
                case 4: sub->can_encrypt = b; break;
                case 5: sub->can_sign = b; break;
                case 6: sub->can_certify = b; break;
                case 7: sub->secret = b; break;
                case 8: sub->can_auth = b; break;
                case 11: sub->algo = atoi (fields[f]); break;
                case 12: sub->bits = atoi (fields[f]); break;
                case 13: sub->keyid = pwmd_strdup (fields[f]); break;
                case 14: sub->fpr = pwmd_strdup (fields[f]); break;
                case 15: sub->grip = pwmd_strdup (fields[f]); break;
                case 16: sub->created = strtoul (fields[f], NULL, 10); break;
                case 17: sub->expires = strtoul (fields[f], NULL, 10); break;
                case 18: sub->card = fields[f] && strlen(fields[f]) > 1
                         ? pwmd_strdup (fields[f]) : NULL; break;
                case 19: sub->curve = strlen (fields[f]) > 1 ? pwmd_strdup (openpgp_unescape(fields[f])) : NULL; break;
                default:
                  break;
                }
            }

          tlist = slist_append (key->subkeys, sub);
          if (!tlist)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          key->subkeys = tlist;
        }

      if (rc)
        {
          strv_free (fields);
          break;
        }

      // Re-create a line containing the userIds.
      for (; f < strv_length (fields); f++)
        {
          struct userid_s *userid;

          userid = pwmd_calloc (1, sizeof (struct userid_s));
          if (!userid)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          // Revoked.
          f++;
          // Invalid.
          f++;
          // Validity.
          f++;

          userid->userid = pwmd_strdup (openpgp_unescape (fields[f++]));
          userid->name = pwmd_strdup (openpgp_unescape (fields[f++]));
          userid->email = pwmd_strdup (openpgp_unescape (fields[f++]));
          userid->comment = pwmd_strdup (openpgp_unescape (fields[f]));

          tlist = slist_append (key->userids, userid);
          if (!tlist)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          key->userids = tlist;
        }

      strv_free (fields);
      if (rc)
        break;

      tlist = slist_append (keys, key);
      if (!tlist)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      keys = tlist;
    }

  strv_free (lines);

  if (!rc)
    display_listkeys (keys);

  free_listkeys (keys);
  return rc;
}

static gpg_error_t
parse_dotcommand (const char *line, char **result,
		  size_t * len, struct inquire_s *inq)
{
  const char *p = line;
  gpg_error_t rc = 0;

  if (!strncmp (p, ".read", 5) && (p[5] == ' ' || p[5] == 0))
    rc = read_command (p + 5, result, len);
  else if (!strncmp (p, ".redir", 6) && (p[6] == ' ' || p[6] == 0))
    rc = redir_command (p + 6);
  else if (!strncmp (p, ".help", 5) && (p[5] == ' ' || p[5] == 0))
    rc = help_command (p + 5);
  else if (!strncmp (p, ".open", 5) && (p[5] == ' ' || p[5] == 0))
    rc = open_command ((char *)p + 5);
  else if (!strncmp (p, ".set", 4) && (p[4] == ' ' || p[4] == 0))
    rc = set_command (p + 4);
  else if (!strncmp (p, ".save", 5) && (p[5] == ' ' || p[5] == 0))
    rc = do_save_passwd_command (p + 5, SAVE_WHICH_SAVE);
  else if (!strncmp (p, ".passwd", 7) && (p[7] == ' ' || p[7] == 0))
    rc = do_save_passwd_command (p + 7, SAVE_WHICH_PASSWD);
  else if (!strncmp (p, ".genkey", 7) && (p[7] == ' ' || p[7] == 0))
    rc = do_save_passwd_command (p + 7, SAVE_WHICH_GENKEY);
  else if (!strncmp (p, ".listkeys", 9) && (p[9] == ' ' || p[9] == 0))
    rc = listkeys_command (p+9);
  else
    {
      rc = pwmd_command (pwm, result, len, inquire_cb, inq, "%s", line);
#ifdef HAVE_LIBREADLINE
      if (interactive)
        reset_keyfiles ();
#else
      reset_keyfiles ();
#endif

      if (!rc && !strncasecmp (line, "RESET", 5) && (p[5] == ' ' || p[5] == 0))
        {
          pwmd_free (filename);
          filename = NULL;
        }
    }

  return FINISH (rc);
}

#ifdef HAVE_LIBREADLINE
#ifdef HAVE_READLINE_HISTORY
static void
add_history_item (const char *line)
{
  int i;
  HIST_ENTRY *h = NULL;

  for (i = 0; i < history_length; i++)
    {
      h = history_get (i + history_base);
      if (h && !strcmp (h->line, line))
        break;
    }

  if (h)
    {
      char *s = NULL;

      h = remove_history (i);
#ifdef HAVE_FREE_HISTORY_ENTRY
      s = free_history_entry (h);
#endif
      if (s)
        wipememory (s, 0, strlen (s));
      free (s);
    }

  add_history (line);
}
#endif

static gpg_error_t
do_interactive ()
{
  gpg_error_t rc;
  struct inquire_s *inq = NULL;

  rl_initialize ();
  rc = process_cmd ();
  if (rc)
    return rc;

  rc = set_inquire (STDIN_FILENO, NULL, &inq);
  if (rc)
    return rc;

  fprintf (stderr,
	   N_("WARNING: interactive mode doesn't use secure memory!\n"));
  print_help ();
  rl_event_hook = &interactive_hook;
  rl_getc_function = get_readline_char;
  rl_set_keyboard_input_timeout (100000);

  for (;;)
    {
      char *line;
      char *result = NULL;
      size_t len;
      char buf[255];

      snprintf (buf, sizeof (buf), "pwmc%s%s> ",
                filename ? ":" : "", filename ? filename : "");
      line = readline (buf);
      if (interactive_error)
	{
          if (line)
            wipememory (line, 0, strlen (line));

          free (line);
	  rc = interactive_error;
	  break;
	}

      if (!line)
	{
	  rc = finalize ();
	  if (!rc)
	    break;

	  if (gpg_err_code (rc) != GPG_ERR_CANCELED &&
	      gpg_err_code (rc) != GPG_ERR_EOF)
	    fprintf (stderr, "ERR %i: %s\n", rc, gpg_strerror (rc));

	  continue;
	}
      else if (!*line)
	{
	  free (line);
	  continue;
	}

#ifdef HAVE_READLINE_HISTORY
      add_history_item (line);
#endif
      rc = parse_dotcommand (line, &result, &len, inq);
      wipememory (line, 0, strlen (line));
      free (line);
      if (rc)
	{
	  char *tmp = NULL;

	  if (gpg_err_code (rc) == GPG_ERR_BAD_DATA)
	    (void) pwmd_command (pwm, &tmp, NULL, NULL, NULL,
				 "GETINFO last_error");

	  show_error (pwm, rc, tmp);
	  pwmd_free (tmp);
	}
      else if (result && len)
	printf ("%s%s", result, result[len - 1] != '\n' ? "\n" : "");

      pwmd_free (result);
    }

  free_inquire (inq);
  return rc;
}
#endif

static gpg_error_t
finalize ()
{
  gpg_error_t rc = 0;
#ifdef HAVE_LIBREADLINE
  int quit = 0;

  if (interactive)
    {
      int finished = 0;

      fprintf (stderr, "\n");

      do
	{
          char *p, buf[16];

	  fprintf (stderr,
		   N_
		   ("(c)ancel/(f)orget password/(s)ave/(Q)uit/(S)ave and quit/(h)elp?: "));
	  p = fgets (buf, sizeof (buf), stdin);

	  if (feof (stdin))
	    {
	      clearerr (stdin);
	      return GPG_ERR_EOF;
	    }

	  switch (*p)
	    {
	    case 'h':
	      print_help ();
	      break;
	    case 'c':
	      return GPG_ERR_CANCELED;
	    case 'Q':
	      return 0;
	    case 'f':
	      rc = pwmd_command (pwm, NULL, NULL, NULL, NULL,
				 "CLEARCACHE %s", filename);
	      if (rc)
		return rc;

	      interactive_hook ();
	      break;
	    case 'S':
	      quit = 1;
	    case 's':
	      save = 1;
	      finished = 1;
	      break;
	    default:
	      break;
	    }
	}
      while (!finished);
    }
#endif

  if (save && !filename)
    {
      fprintf (stderr,
	       N_
	       ("No filename was specified on the command line. Aborting.\n"));
      return GPG_ERR_CANCELED;
    }

  if (save && filename)
    {
      char *args =
	pwmd_strdup_printf ("%s %s%s %s%s %s",
                            symmetric ? "--symmetric" : "",
			    keyid ? "--keyid=" : "",
			    keyid ? keyid : "",
			    sign_keyid ? "--sign-keyid=" : "",
			    sign_keyid ? sign_keyid : "",
			    keyparams ? "--inquire-keyparam"  : "");

#ifdef HAVE_LIBREADLINE
      if (!quiet || interactive)
	{
#else
      if (!quiet)
	{
#endif
	  fprintf (stderr, "\n");
	  fprintf (stderr, N_("Saving changes ...\n"));
	}

      rc = save_command (args);
      pwmd_free (args);
    }

#ifdef HAVE_LIBREADLINE
  if (interactive)
    return rc ? rc : quit ? 0 : GPG_ERR_CANCELED;
#endif

  return rc;
}

static void
parse_status_ignore (char *str)
{
  char *s;

  strv_free (status_ignore);
  status_ignore = NULL;
  if (!str || !*str)
    return;

  while ((s = strsep (&str, ",")))
    {
      char **p = strv_cat (status_ignore, pwmd_strdup (s));

      if (!p)
        {
          strv_free (status_ignore);
          status_ignore = NULL;
          break;
        }
      status_ignore = p;
    }
}

int
main (int argc, char *argv[])
{
  int connected = 0;
  int status_state = 0;
  gpg_error_t rc;
  int opt;
  char command[ASSUAN_LINELENGTH], *p = NULL;
  char *result = NULL;
  size_t len = 0;
  char *display = NULL, *tty = NULL, *ttytype = NULL;
  char *lcctype = NULL, *lcmessages = NULL;
  int outfd = STDOUT_FILENO;
  FILE *outfp = stdout;
  int show_status = 1;
  const char *clientname = "pwmc";
  char *inquire = NULL;
  char *inquire_line = NULL;
  int timeout = 0;
#ifdef WITH_SSH
  int use_ssh_agent = -1;
  char *knownhosts = NULL;
  char *identity = NULL;
  int needs_passphrase = 0;
  char *ssh_passphrase_file = NULL;
#endif
#ifdef WITH_GNUTLS
  char *cacert = NULL;
  char *clientcert = NULL;
  char *clientkey = NULL;
  char *prio = NULL;
  int tls_verify = -1;
  char *tls_fingerprint = NULL;
#endif
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  pwmd_socket_t socktype;
  long socket_timeout = 300;
  int connect_timeout = 120;
#endif
#ifdef HAVE_LIBREADLINE
  int no_interactive = 0;
#endif
  int lock_on_open = -1;
  long lock_timeout = -2;
  char *url = NULL;
  char *tmp = NULL;
  /* The order is important. */
  enum
  {
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
    OPT_SOCKET_TIMEOUT, OPT_CONNECT_TIMEOUT,
#endif
#ifdef WITH_SSH
    OPT_USE_SSH_AGENT, OPT_IDENTITY, OPT_KNOWNHOSTS, OPT_SSH_NEEDS_PASSPHRASE,
    OPT_SSH_PASSPHRASE_FILE,
#endif
#ifdef WITH_GNUTLS
    OPT_CACERT, OPT_CLIENTCERT, OPT_CLIENTKEY, OPT_PRIORITY, OPT_VERIFY,
    OPT_SERVER_FP,
#endif
    OPT_URL, OPT_SOCKET, OPT_LOCAL, OPT_TTYNAME, OPT_TTYTYPE, OPT_DISPLAY,
    OPT_LC_CTYPE, OPT_LC_MESSAGES, OPT_TIMEOUT, OPT_TRIES, OPT_PINENTRY,
    OPT_KEYFILE, OPT_PASSPHRASE_FILE, OPT_NEW_KEYFILE, OPT_NEW_PASSPHRASE_FILE,
    OPT_SIGN_KEYFILE, OPT_SIGN_PASSPHRASE_FILE, OPT_NOLOCK, OPT_LOCK_TIMEOUT,
    OPT_SAVE, OPT_OUTPUT_FD, OPT_INQUIRE, OPT_INQUIRE_FD, OPT_INQUIRE_FILE,
    OPT_INQUIRE_LINE, OPT_NO_STATUS, OPT_STATUS_IGNORE, OPT_STATUSFD, OPT_NAME,
    OPT_VERSION, OPT_HELP, OPT_KEYID, OPT_SIGN_KEYID, OPT_SYMMETRIC,
    OPT_KEYPARAMS, OPT_NO_PINENTRY, OPT_QUIET, OPT_STATUS_STATE,
#ifdef HAVE_LIBREADLINE
    OPT_NO_INTERACTIVE,
#endif
  };
  const struct option long_opts[] = {
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
    {"socket-timeout", 1, 0, 0},
    {"connect-timeout", 1, 0, 0},
#endif

#ifdef WITH_SSH
    {"no-ssh-agent", 0, 0, 0},
    {"identity", 1, 0, 'i'},
    {"knownhosts", 1, 0, 'k'},
    {"ssh-needs-passphrase", 0, 0, 0},
    {"ssh-passphrase-file", 1, 0, 0},
#endif
#ifdef WITH_GNUTLS
    {"ca-cert", 1, 0, 0},
    {"client-cert", 1, 0, 0},
    {"client-key", 1, 0, 0},
    {"tls-priority", 1, 0, 0},
    {"no-tls-verify", 0, 0, 0},
    {"tls-fingerprint", 1, 0, 0},
#endif
    {"socket", 1, 0, 0},
    {"url", 1, 0, 0},
    {"local-pinentry", 0, 0},
    {"ttyname", 1, 0, 'y'},
    {"ttytype", 1, 0, 't'},
    {"display", 1, 0, 'd'},
    {"lc-ctype", 1, 0, 0},
    {"lc-messages", 1, 0, 0},
    {"timeout", 1, 0, 0},
    {"tries", 1, 0, 0},
    {"pinentry", 1, 0, 0},
    {"key-file", 1, 0, 0},
    {"passphrase-file", 1, 0, 0},
    {"new-key-file", 1, 0, 0},
    {"new-passphrase-file", 1, 0, 0},
    {"sign-key-file", 1, 0, 0},
    {"sign-passphrase-file", 1, 0, 0},
    {"no-lock", 0, 0, 0},
    {"lock-timeout", 1, 0, 0},
    {"save", 0, 0, 'S'},
    {"output-fd", 1, 0, 0},
    {"inquire", 1, 0, 0},
    {"inquire-fd", 1, 0, 0},
    {"inquire-file", 1, 0, 0},
    {"inquire-line", 1, 0, 'L'},
    {"no-status", 0, 0, 0},
    {"status-ignore", 1, 0, 0},
    {"status-fd", 1, 0, 0},
    {"name", 1, 0, 'n'},
    {"version", 0, 0, 0},
    {"help", 0, 0, 0},
    {"keyid", 1, 0, 0},
    {"sign-keyid", 1, 0, 0},
    {"symmetric", 0, 0, 0},
    {"key-params", 1, 0, 0},
    {"no-pinentry", 0, 0, 0},
    {"quiet", 0, 0, 0},
    {"status-state", 0, 0, 0},
#ifdef HAVE_LIBREADLINE
    {"no-interactive", 0, 0},
#endif
    {0, 0, 0, 0}
  };
#ifdef WITH_SSH
  const char *optstring = "L:y:t:d:P:I:Sn:i:k:s";
#else
  const char *optstring = "L:y:t:d:P:I:Sn:s";
#endif
  int opt_index = 0;

#ifdef ENABLE_NLS
  setlocale (LC_ALL, "");
  bindtextdomain ("libpwmd", LOCALEDIR);
#endif

  tries = DEFAULT_PIN_TRIES;
  inquirefd = STDIN_FILENO;
  statusfd = STDERR_FILENO;
  statusfp = stderr;
  tmp = pwmd_strdup (DEFAULT_STATUS_IGNORE);
  parse_status_ignore (tmp);
  pwmd_free (tmp);
  no_pinentry = -1;
  local_pin = -1;

  while ((opt =
	  getopt_long (argc, argv, optstring, long_opts, &opt_index)) != -1)
    {
      switch (opt)
	{
	  /* Handle long options without a short option part. */
	case 0:
	  switch (opt_index)
	    {
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
	    case OPT_SOCKET_TIMEOUT:
	      socket_timeout = strtol (optarg, &p, 10);
	      break;
	    case OPT_CONNECT_TIMEOUT:
	      connect_timeout = strtol (optarg, &p, 10);
	      break;
#endif
#ifdef WITH_SSH
	    case OPT_USE_SSH_AGENT:
	      use_ssh_agent = 0;
	      break;
            case OPT_SSH_NEEDS_PASSPHRASE:
	      needs_passphrase = 1;
	      break;
            case OPT_SSH_PASSPHRASE_FILE:
              ssh_passphrase_file = optarg;
              break;
#endif
#ifdef WITH_GNUTLS
	    case OPT_CACERT:
	      cacert = optarg;
	      break;
	    case OPT_CLIENTCERT:
	      clientcert = optarg;
	      break;
	    case OPT_CLIENTKEY:
	      clientkey = optarg;
	      break;
	    case OPT_PRIORITY:
	      prio = optarg;
	      break;
	    case OPT_VERIFY:
	      tls_verify = 0;
	      break;
	    case OPT_SERVER_FP:
	      tls_fingerprint = optarg;
	      break;
#endif
            case OPT_SYMMETRIC:
              symmetric = 1;
              break;
	    case OPT_KEYPARAMS:
	      keyparams = optarg;
	      break;
	    case OPT_KEYFILE:
            case OPT_PASSPHRASE_FILE:
	      keyfile = pwmd_strdup (optarg);
	      break;
	    case OPT_NEW_KEYFILE:
            case OPT_NEW_PASSPHRASE_FILE:
	      new_keyfile = pwmd_strdup (optarg);
	      break;
            case OPT_SIGN_KEYFILE:
            case OPT_SIGN_PASSPHRASE_FILE:
              sign_keyfile = pwmd_strdup (optarg);
              break;
	    case OPT_NOLOCK:
	      lock_on_open = 0;
	      break;
	    case OPT_LOCK_TIMEOUT:
	      lock_timeout = strtol (optarg, &p, 10);
	      break;
	    case OPT_URL:
            case OPT_SOCKET:
	      url = optarg;
	      break;
	    case OPT_LOCAL:
	      local_pin = 1;
	      break;
	    case OPT_LC_CTYPE:
	      lcctype = pwmd_strdup (optarg);
	      break;
	    case OPT_LC_MESSAGES:
	      lcmessages = pwmd_strdup (optarg);
	      break;
	    case OPT_TIMEOUT:
	      timeout = strtol (optarg, &p, 10);
	      break;
	    case OPT_TRIES:
	      tries = strtol (optarg, &p, 10);
	      break;
	    case OPT_INQUIRE:
	      inquire = escape (optarg);
	      break;
	    case OPT_INQUIRE_FD:
	      inquirefd = strtol (optarg, &p, 10);
	      break;
	    case OPT_INQUIRE_FILE:
	      inquirefd = open (optarg, O_RDONLY);
	      if (inquirefd == -1)
		err (EXIT_FAILURE, "%s", optarg);
	      break;
	    case OPT_OUTPUT_FD:
	      outfd = strtol (optarg, &p, 10);
	      if (!p || !*p)
		{
		  outfp = fdopen (outfd, "wb");
		  if (!outfp)
		      err (EXIT_FAILURE, "%i", outfd);
		}
	      break;
	    case OPT_NO_STATUS:
	      show_status = 0;
	      break;
            case OPT_STATUSFD:
	      statusfd = strtol (optarg, &p, 10);
	      if (!p || !*p)
		{
		  statusfp = fdopen (statusfd, "wb");
		  if (!statusfp)
		      err (EXIT_FAILURE, "%i", statusfd);
		}
              break;
            case OPT_STATUS_IGNORE:
              parse_status_ignore (optarg);
              break;
	    case OPT_VERSION:
	      printf ("%s (pwmc)\n\n"
		      "Copyright (C) 2006-2025\n"
		      "%s\n"
		      "Released under the terms of the LGPL v2.1.\n\n"
		      "Compile-time features:\n"
#ifdef HAVE_LIBREADLINE
		      "+INTERACTIVE "
#else
		      "-INTERACTIVE "
#endif
#ifdef WITH_SSH
		      "+SSH "
#else
		      "-SSH "
#endif
#ifdef WITH_GNUTLS
		      "+GNUTLS "
#else
		      "-GNUTLS "
#endif
#ifdef WITH_PINENTRY
		      "+PINENTRY "
#else
		      "-PINENTRY "
#endif
#ifdef WITH_QUALITY
		      "+QUALITY "
#else
		      "-QUALITY "
#endif
#ifdef MEM_DEBUG
		      "+MEM_DEBUG "
#else
		      "-MEM_DEBUG "
#endif
		      "\n", PACKAGE_STRING LIBPWMD_GIT_HASH, PACKAGE_BUGREPORT);
	      exit (EXIT_SUCCESS);
	    case OPT_PINENTRY:
	      break;
	    case OPT_HELP:
	      usage (argv[0], EXIT_SUCCESS);
	    case OPT_KEYID:
	      keyid = optarg;
	      break;
	    case OPT_SIGN_KEYID:
	      sign_keyid = optarg;
	      break;
	    case OPT_QUIET:
	      quiet = 1;
	      show_status = 0;
	      break;
            case OPT_STATUS_STATE:
              status_state = 1;
              break;
	    case OPT_NO_PINENTRY:
	      no_pinentry = 1;
	      break;
#ifdef HAVE_LIBREADLINE
	    case OPT_NO_INTERACTIVE:
	      no_interactive = 1;
	      break;
#endif
	    default:
	      usage (argv[0], EXIT_FAILURE);
	    }

	  if (p && *p)
	    {
	      fprintf (stderr, N_("%s: invalid argument for option '--%s'\n"),
		       argv[0], long_opts[opt_index].name);
	      usage (argv[0], EXIT_FAILURE);
	    }

	  break;
#ifdef WITH_SSH
	case 'i':
	  identity = optarg;
	  break;
	case 'k':
	  knownhosts = optarg;
	  break;
#endif
	case 'L':
	  inquire_line = optarg;
	  break;
	case 'y':
	  tty = optarg;
	  break;
	case 't':
	  ttytype = optarg;
	  break;
	case 'd':
	  display = optarg;
	  break;
	case 'S':
	  save = 1;
	  break;
	case 'n':
	  clientname = optarg;
	  break;
	default:
	  usage (argv[0], EXIT_FAILURE);
	}
    }

#ifdef HAVE_LIBREADLINE
  if (isatty (STDIN_FILENO) && !inquire && !inquire_line && !no_interactive)
    interactive = 1;
#endif

  pwmd_init ();
  rc = pwmd_new (clientname, &pwm);
  if (rc)
    goto done;

  filename = argv[optind] ? pwmd_strdup (argv[optind]) : NULL;
  if (no_pinentry != -1)
    rc = pwmd_setopt (pwm, PWMD_OPTION_NO_PINENTRY, no_pinentry);
  else
    rc = pwmd_getopt (pwm, PWMD_OPTION_NO_PINENTRY, &no_pinentry);
  if (rc)
    goto done;

  if (local_pin != -1)
    rc = pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, local_pin);
  else
    rc = pwmd_getopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, &local_pin);
  if (rc)
    goto done;

  pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TRIES, tries);
  if (!quiet)
    fprintf (stderr, N_("Connecting ...\n"));

  if (status_state)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_STATE_STATUS, 1);
      if (rc)
	goto done;
    }

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  socktype = is_remote_url (url);
  if (socktype != PWMD_SOCKET_LOCAL)
    {
      local_pin = 1;
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
      rc = pwmd_setopt (pwm, PWMD_OPTION_SOCKET_TIMEOUT, connect_timeout);
      if (rc)
	goto done;
#endif

      if (socktype == PWMD_SOCKET_SSH)
	{
#ifdef WITH_SSH
          if (use_ssh_agent == -1)
            rc = pwmd_getopt (pwm, PWMD_OPTION_SSH_AGENT, &use_ssh_agent);

          if (!rc)
            {
              if (use_ssh_agent != 0)
                {
                  char *t = getenv ("SSH_AUTH_SOCK");

                  use_ssh_agent = t && *t ? 1 : 0;
                }

              if (identity)
                use_ssh_agent = 0;

              if (!identity && !use_ssh_agent)
                {
                  pwmd_close (pwm);
                  fprintf (stderr, N_("No identity specified and no SSH_AUTH_SOCK environment variable.\n"));
                  usage (argv [0], EXIT_FAILURE);
                }

              rc = pwmd_setopt (pwm, PWMD_OPTION_SSH_AGENT, use_ssh_agent);
              if (rc)
                goto done;
            }
          else
            goto done;

          rc = pwmd_setopt (pwm, PWMD_OPTION_SSH_NEEDS_PASSPHRASE,
                            needs_passphrase);
          if (rc)
            goto done;

          if (ssh_passphrase_file)
            {
              struct stat st;
              int fd;

              if (stat (ssh_passphrase_file, &st) == -1)
                {
                  rc = gpg_error_from_syserror ();
                  fprintf(stderr, "%s: %s\n", ssh_passphrase_file,
                          gpg_strerror (rc));
                  goto done;
                }

              result = pwmd_calloc (1, st.st_size+1);
              if (!result)
                {
                  rc = GPG_ERR_ENOMEM;
                  goto done;
                }

              fd = open (ssh_passphrase_file, O_RDONLY);
              if (fd != -1)
                {
                  len = read (fd, result, st.st_size);
                  if (len == st.st_size)
                    rc = pwmd_setopt (pwm, PWMD_OPTION_SSH_PASSPHRASE, result);
                  else
                    rc = gpg_error_from_syserror ();

                  close (fd);
                }
              else
                rc = gpg_error_from_syserror ();

              pwmd_free (result);
              result = NULL;
              len = 0;

              if (rc)
                {
                  fprintf(stderr, "%s: %s\n", ssh_passphrase_file,
                          gpg_strerror (rc));
                  goto done;
                }
            }

          rc = pwmd_connect (pwm, url, identity, knownhosts);
#endif
	}
#ifdef WITH_GNUTLS
      else
	{
          if (tls_verify != -1)
            {
              rc = pwmd_setopt (pwm, PWMD_OPTION_TLS_VERIFY, tls_verify);
              if (rc)
                goto done;
            }

          if (prio)
            rc = pwmd_setopt (pwm, PWMD_OPTION_TLS_PRIORITY, prio);

          if (!rc)
            rc = pwmd_connect (pwm, url, clientcert, clientkey, cacert,
                               tls_fingerprint);
	}
#endif
    }
  else
    rc = pwmd_connect (pwm, url);
#else
  rc = pwmd_connect (pwm, url);
#endif
  if (rc)
    goto done;

  if (!quiet)
    fprintf (stderr, N_("Connected.\n"));

  connected = 1;
#if defined (WITH_SSH) || defined (WITH_GNUTLS)
  rc = pwmd_setopt (pwm, PWMD_OPTION_SOCKET_TIMEOUT, socket_timeout);
  if (rc)
    goto done;
#endif

  if (lock_on_open != -1)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_ON_OPEN, lock_on_open);
      if (rc)
	goto done;
    }

  if (lock_timeout != -2)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_TIMEOUT, lock_timeout);
      if (rc)
	goto done;
    }

  rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DESC, NULL, NULL);
  if (rc)
    goto done;

  if (timeout > 0)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TIMEOUT, timeout);
      if (rc)
	goto done;
    }

  if (display)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DISPLAY, display);
      if (rc)
	goto done;
    }

  if (tty)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TTY, tty);
      if (rc)
	goto done;
    }

  if (ttytype)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TERM, ttytype);
      if (rc)
	goto done;
    }

  if (lcctype)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_LC_CTYPE, lcctype);
      if (rc)
	goto done;
    }

  if (lcmessages)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_LC_MESSAGES, lcmessages);
      if (rc)
	goto done;
    }

  if (show_status)
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_STATUS_CB, status_msg_cb);
      if (rc)
	goto done;
    }

  if (filename)
    {
      rc = open_command (filename);
      if (rc)
	goto done;
    }

#ifdef HAVE_LIBREADLINE
  if (interactive)
    {
      rc = do_interactive ();
      result = NULL;
      while (history_length)
        {
          char *s = NULL;
          HIST_ENTRY *h = remove_history (0);
#ifdef HAVE_FREE_HISTORY_ENTRY
          s = free_history_entry (h);
#else
          (void)h;
#endif
          if (s)
            wipememory (s, 0, strlen (s));
          free (s);
        }
      goto do_exit;
    }
#endif

  if (inquire)
    {
      struct inquire_s *inq = NULL;

      rc = set_inquire (inquirefd, inquire_line, &inq);
      if (!rc)
	rc = pwmd_command (pwm, &result, &len, inquire_cb, inq, "%s", inquire);

      free_inquire (inq);
      goto done;
    }

#ifndef __MINGW32__
  if (fcntl (STDIN_FILENO, F_SETFL, O_NONBLOCK, 1) == -1)
    {
      rc = gpg_error_from_errno (errno);
      goto done;
    }
#endif

  ssize_t n;

  for (;;)
    {
      rc = process_cmd ();

      if (rc)
	goto done;

      n = read (STDIN_FILENO, command, sizeof (command)-1);
      if (n == -1)
	{
	  if (errno == EAGAIN)
	    {
#ifdef __MINGW32__
	      Sleep (100000);
#else
	      usleep (100000);
#endif
	      continue;
	    }

	  rc = gpg_error_from_errno (errno);
	  goto done;
	}
      else if (!n)
	goto done;

      p = command;
      command[n] = 0;
      break;
    }

  if (!p || !*p || !strcasecmp (p, "BYE"))
    goto done;


  struct inquire_s *inq = NULL;
  rc = set_inquire (inquirefd, inquire_line, &inq);
  if (!rc)
    {
      rc = parse_dotcommand (command, &result, &len, inq);
      free_inquire (inq);
    }

  if (rc)
    goto done;

done:
  if (result)
    {
      fwrite (result, 1, result[len - 1] == 0 ? len - 1 : len, outfp);
      fflush (outfp);
      pwmd_free (result);
    }

  result = NULL;
  if (!rc)
    rc = finalize ();
  else if (gpg_err_code (rc) == GPG_ERR_BAD_DATA)
    (void) pwmd_command (pwm, &result, NULL, NULL, NULL,
			 "GETINFO last_error");

#ifdef HAVE_LIBREADLINE
do_exit:
#endif
  wipememory (command, 0, sizeof (command));

  if (rc && !quiet)
    show_error (pwm, rc, result);

  pwmd_close (pwm);
  reset_keyfiles ();
  pwmd_deinit ();
  pwmd_free (result);
  pwmd_free (filename);
  pwmd_free (inquire);
  strv_free (status_ignore);
  if (connected && !quiet)
    fprintf (stderr, N_("Connection closed.\n"));

  exit (rc ? EXIT_FAILURE : EXIT_SUCCESS);
}
